package com.rzx.deviceiniter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rzx.deviceiniter.appinstall.AppInstallManager;
import com.rzx.deviceiniter.cmd.RootCmd;
import com.rzx.deviceiniter.ui.FxService;
import com.rzx.deviceiniter.ui.runningman.CustomProgressDialog;
import com.rzx.deviceiniter.util.Assets;
import com.rzx.deviceiniter.util.FileUtils;
import com.rzx.deviceiniter.util.Md5Util;
import com.rzx.deviceiniter.util.ToastUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String apkPath = "/mnt/sdcard/tmp/apk/";
    private final static String luaPath = "/mnt/sdcard/GodHand/";
    private final static String binPath = "/mnt/sdcard/tmp/bin/";
    private final static String UUIDFile = "mnt/sdcard/GodHand/uuid.txt";
    public static final int MSG_TOAST = 0x1000001;
    private CustomProgressDialog progressDialog = null;

    private final Handler msgHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.arg1) {
                case MSG_TOAST:
                    Toast.makeText(getApplicationContext(), msg.getData().getString("text"), Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
        }
    };

    public void toast(String text) {
        Message msg = msgHandler.obtainMessage();
        msg.arg1 = MSG_TOAST;
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        msg.setData(bundle);
        msgHandler.sendMessage(msg);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ToastUtil.msgHandler = msgHandler;
        Button initBtn = (Button) findViewById(R.id.btn_init);
        initBtn.setOnClickListener(this);
        TelephonyManager telephonyManager=(TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String imei=telephonyManager.getDeviceId();
        Toast.makeText(this.getApplicationContext(), imei, Toast.LENGTH_LONG).show();

        TextView txtVersion = (TextView) findViewById(R.id.txt_version);
        TextView txtDetail = (TextView) findViewById(R.id.txt_detail);

        progressDialog = new CustomProgressDialog(MainActivity.this, "正在初始化中",R.drawable.frame);

        InputStream is1 = null, is2 = null;
        try {
            is1 = getAssets().open("godhand/VERSION");
            txtVersion.setText(inputStream2String(is1));
            is2 = getAssets().open("godhand/README");
            txtDetail.setText(inputStream2String(is2));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is1 != null)
                    is1.close();
                if (is2 != null)
                    is2.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String inputStream2String (InputStream in) throws IOException {
        StringBuffer out = new StringBuffer();
        byte[] b = new byte[4096];
        for (int n; (n = in.read(b)) != -1;) {
            out.append(new String(b, 0, n));
        }
        return out.toString();
    }

    private void initPhone() {

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
//                AppInstallManager.uninstallAllUser(MainActivity.this);
                ToastUtil.get().toast("停止应用程序");
                SystemClock.sleep(2000);
                RootCmd.execRootCmd("am force-stop com.rzx.godhandmator");
                RootCmd.execRootCmd("am force-stop com.rzx.godhandmator.test");
                RootCmd.execRootCmd("busybox ps -ef|grep rzx_runner|busybox grep -v 'grep'|busybox awk '{print $1}'|busybox xargs kill -9");
                RootCmd.execRootCmd("busybox ps -ef|grep rzx_maintaince|busybox grep -v 'grep'|busybox awk '{print $1}'|busybox xargs kill -9");
                RootCmd.execRootCmd("mkdir -p /mnt/sdcard/GodHand/tmp/fb_info");

                // 删除apkPath所有文件
                RootCmd.execRootCmd("rm -fr " + apkPath);

                ToastUtil.get().toast("安装apk文件");
                // apk写入sdcard
                Assets.CopyAssets(MainActivity.this, "apk", apkPath);
                // 安装apk
                AppInstallManager.installDir(apkPath, "apk");
//                RootCmd.execRootCmd("pm install -r /mnt/sdcard/GodHand/res/facebook.apk");

                ToastUtil.get().toast("安装bin文件");
                // 安装busybox等相关文件
                Assets.CopyAssets(MainActivity.this, "bin", binPath);
                AppInstallManager.installDir(binPath, "bin");

                ToastUtil.get().toast("安装脚本文件");
                // 脚本 写入sdcard
                RootCmd.execRootCmd("rm -fr /sdcard/GodHand/lua/");
                RootCmd.execRootCmd("rm -fr /sdcard/GodHand/log/");
//                RootCmd.execRootCmd("rm -fr /sdcard/GodHand/res/*.pro*");
                RootCmd.execRootCmd("rm -fr /sdcard/GodHand/tmp/");
                Assets.CopyAssets(MainActivity.this, "godhand", luaPath);

                // 创建UUID
                setUUID();

                ToastUtil.get().toast("初始化完成");
                if (progressDialog.isShowing()) {
                    progressDialog.cancel();
                }
//                Intent intent = new Intent(getApplicationContext(), FxService.class);
//                stopService(intent);
            }
        }).start();

    }

    @Override
    public void onClick(View v) {
       switch (v.getId()){
           case R.id.btn_init:
               Intent intent = new Intent(this, FxService.class);
//               startService(intent);
               progressDialog.show();
               initPhone();
               break;
       }
    }

    private String setUUID() {
        String imei = RootCmd.execRootCmd("dumpsys iphonesubinfo");
        String buildpro = RootCmd.execRootCmd("md5  /system/build.prop");

        String uuid = Md5Util.Md5_32(imei + buildpro);
        FileUtils.writeFile(UUIDFile, uuid);
        return uuid;
    }
}
