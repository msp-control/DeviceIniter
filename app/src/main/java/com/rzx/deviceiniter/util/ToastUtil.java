package com.rzx.deviceiniter.util;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.rzx.deviceiniter.MainActivity;

/**
 * Created by Administrator on 2016/11/22.
 */
public class ToastUtil {
    private static ToastUtil toastUtil = null;
    public static Handler msgHandler = null;

    public static ToastUtil get() {
        if (msgHandler == null) {
            return null;
        }

        if(toastUtil == null) {
            toastUtil = new ToastUtil();
        }
        return toastUtil;
    }

    public void toast(String text) {
        Message msg = msgHandler.obtainMessage();
        msg.arg1 = MainActivity.MSG_TOAST;
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        msg.setData(bundle);
        msgHandler.sendMessage(msg);
    }
}
