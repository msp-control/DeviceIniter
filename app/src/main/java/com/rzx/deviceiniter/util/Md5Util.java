package com.rzx.deviceiniter.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Administrator on 2016/11/14.
 */
public class Md5Util {

    /**
     * Md5 32 bit, lowercase letters.
     *
     * @param str
     * @return
     */
    public static String Md5_32(String str){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            byte b[] = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            return buf.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Md5 16 bit, lowercase letters.
     *
     * @param str
     * @return
     */
    public static String Md5_16(String str){
        String ret = Md5_32(str);
        if (ret != null){
            return ret.substring(8, 24);
        }

        return null;
    }
}
