require 'GHLib'
require 'SharedDic'
require 'ProtocolDefine'
require 'TaskInterface'
json = require 'json'

local local_ip = "127.0.0.1"
local udp_port = 12580
local socket = require("socket")
local udpsocket = socket.udp4()

local task_file_recv_path = getPath().."/tmp/task_file.tar"
local task_file_extract_path = getPath().."/tmp/gh_task"
local update_file_recv_path = getPath().."/tmp/ts_code_update.tar"

function check_log_size()
	local file_list = getFileList(getPath().."/log")
	for k,v in pairs(file_list) do
		if(lengthOfFile(v) > 1024*500) then
			os.remove(v)
		end
	end
end

function main()
	--检查日志大小,超过500KB就删除文件
	check_log_size()
	
	local guid = AutomatorApi:readFile(getPath().."/uuid.txt")
	
	--获取任务文件,如果上一个任务没有正常执行完毕，就重新执行上一个任务
	if AutomatorApi:fileExists(task_file_recv_path) == false then
		TaskInterface.downloadTaskFile(task_file_recv_path, guid)
	end

	if AutomatorApi:fileExists(task_file_recv_path) == false then
		AutomatorApi:log("main", "Get task file failed. Failed connect to http host.")
		return
	end
	
	local len_file = lengthOfFile(task_file_recv_path)
	if len_file < 20 then
		local f_data = AutomatorApi:readFile(task_file_recv_path)
		if string.find(f_data, '{"success":false}') ~= nil then
			AutomatorApi:log("main", "Get task file failed. Host return false.")
			AutomatorApi:executeShellCommand("rm -f "..task_file_recv_path)
			return
		end
	end

	AutomatorApi:executeShellCommand("mkdir -p "..task_file_extract_path)
	AutomatorApi:executeShellCommand("rm -fr "..task_file_extract_path.."/*")
	AutomatorApi:executeShellCommand("busybox tar -xf "..task_file_recv_path.." -C "..task_file_extract_path)

	local task_id = AutomatorApi:executeShellCommand("ls "..task_file_extract_path)
	task_id = string.gsub(task_id, "\n", "") or ''
  
  TaskInterface.downloadComplete(guid, task_id)
	if task_id == "" then
    local result = {}
    result['status'] = RET_ERROR_TASK
    result['message'] = "Error tar file."
    result["task_id"] = task_id
    result["guid"] = guid	
		TaskInterface.postTaskResult(result)
		return
	end

  local jsdata, tbl_task
	local iRet,sRet = pcall(function () 
    jsdata = AutomatorApi:readFile(task_file_extract_path.."/"..task_id.."/script") 
    tbl_task = json.decode(jsdata)
  end)

  if iRet == false then
    local result = {}
    result['status'] = RET_ERROR_TASK
    result['message'] = "Can't find script file or script file format wrong."
    result["task_id"] = task_id
    result["guid"] = guid	
		TaskInterface.postTaskResult(result)
    return
  end
  
	local task_type = tbl_task["task_type"]
	tbl_task["task_id"] = task_id
	SharedDic.set("task_id", task_id)
	SharedDic.set("task_type", task_type)  
	
	require 'DoTask'
	math.randomseed(os.time())
	DoTask.doTask(tbl_task)
	
end

function _getWechatTestAccounts()
	local file_list = {}
	AutomatorApi:executeShellCommand("ls /data/data/|grep 'com.tencent.mm_' > /sdcard/GodHand/tmp/file_list")
	local s = io.open("/sdcard/GodHand/tmp/file_list")
	if s ~= nil then
		local line
		for line in s:lines() do 
			table.insert(file_list, "/data/data/"..line)
		end
		s:close()
	end
	
	local imei = ""
  local password = ""
	local usrs = {}
	
	for i=1,#file_list do
    AutomatorApi:executeShellCommand('busybox chmod -R 777 '..file_list[i])
--    AutomatorApi:executeShellCommand('chmod 666 '..file_list[i]..'/MicroMsg/systemInfo.cfg')
--    AutomatorApi:executeShellCommand('chmod 666 '..file_list[i]..'/MicroMsg/CompatibleInfo.cfg')
--    AutomatorApi:executeShellCommand('chmod 666 '..file_list[i]..'/password')
		imei = AutomatorApi:getWechatImei(file_list[i])
    local iRet,sRet = pcall( function()
      password = AutomatorApi:readFile(file_list[i]..'/password')
      password = string.gsub(password, "\n", "")
    end)
    if iRet then
      table.insert(usrs, {string.match(file_list[i], '.*/com.tencent.mm_(.*)'), password, imei})
    else
      AutomatorApi:toast(sRet or 'xxxx')
      mSleep(2000)
    end
	end
	
	return usrs
end

-- AutomatorApi:screenOn()
AutomatorApi:toast("任务执行开始")
mSleep(2000)

SharedDic.set('enable_login_by_input', 'true')
local g_wx_test_flag = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'test_wx_switch', 'false')	--微信测试标识
SharedDic.set('test_wx_switch', g_wx_test_flag)

local g_fb_test_flag = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'test_fb_switch', 'false') --facebook测试标识
SharedDic.set('test_fb_switch', g_fb_test_flag)

import 'android.support.test.uiautomator.Configurator'
local key_inject_delay = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'key_inject_delay', '40')
Configurator:getInstance():setKeyInjectionDelay(tonumber(key_inject_delay))

if g_wx_test_flag == 'true' then
  SharedDic.set('enable_login_by_input', 'true')
	require 'DoTask'
  
	local test_account = _getWechatTestAccounts()
	if #test_account == 0 then 
		AutomatorApi:logAppend("test_result", "No account found.")
		return	
	end

  for i=1,#test_account do
   DoTask.doTask({},test_account,i)
   AutomatorApi:mSleep(2000)
  end
--	math.randomseed(os.time())
--	DoTask.doTask({},test_account,math.random(#test_account))
elseif g_fb_test_flag == 'true' then
	require 'DoTask'
  AutomatorApi:executeShellCommand("rm -f "..getPath().."/tmp/cur_fb_email.txt")
  local file_table = readFileTable(getPath().."/res/fb_info.txt")
  AutomatorApi:toast('还剩下'..tostring(#file_table).."个账号待注册")
  mSleep(2000)
  if #file_table == 0 then
      AutomatorApi:toast('资源文件已轮询完毕，请更换文件')
      AutomatorApi:mSleep(2000)
      AutomatorApi:toast('资源文件已轮询完毕，请更换文件')
      AutomatorApi:mSleep(2000)
      AutomatorApi:toast('资源文件已轮询完毕，请更换文件')
      AutomatorApi:mSleep(2000)
      AutomatorApi:toast('资源文件已轮询完毕，请更换文件')
      AutomatorApi:mSleep(2000)
      AutomatorApi:toast('资源文件已轮询完毕，请更换文件')
      AutomatorApi:mSleep(2000)
      AutomatorApi:toast('资源文件已轮询完毕，请更换文件')
      AutomatorApi:mSleep(2000)
      AutomatorApi:toast('资源文件已轮询完毕，请更换文件')
      AutomatorApi:mSleep(2000)
  else
    local retCode = DoTask.doTask({},file_table,1)
    if retCode ~= RET_NORMAL_FAILED then
      AutomatorApi:executeShellCommand("busybox sed -i '1d' "..getPath().."/res/fb_info.txt")
    else
      local filen = getPath()..'/tmp/fbRegFaildTimes.txt'
      local oldTryTimes = 0
      local iRet, sRet = pcall(function()	
        oldTryTimes = AutomatorApi:readFile(filen)
      end)
    
      AutomatorApi:writeFile(filen, tostring(tonumber(oldTryTimes)+1))
      if oldTryTimes == '2' then
        AutomatorApi:executeShellCommand("busybox sed -i '1d' "..getPath().."/res/fb_info.txt")
        AutomatorApi:writeFile(filen, tostring(0))
      end
    end
    if retCode == RET_NEED_SMS_VERIFY_FAILED then
      local leftTime = 1000*60*30
      while leftTime > 0 do
        AutomatorApi:toast('账号注册检测到需要身份验证，暂停半小时，\n还剩'..tostring(leftTime/1000)..'秒')
        leftTime = leftTime - 3000
        AutomatorApi:mSleep(3000)
      end
    end
    AutomatorApi:mSleep(2000)
  end
--  for i=1,#file_table do
--    DoTask.doTask({},file_table,i)
--    AutomatorApi:mSleep(2000)
--  end	
else
	main()
end
AutomatorApi:toast("任务执行结束")
mSleep(2000)

-- AutomatorApi:screenOff()