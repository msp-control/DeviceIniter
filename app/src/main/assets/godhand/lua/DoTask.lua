require 'CommonOps'
require 'WechatOps'
require 'FacebookOps'

DoTask = {}

function DoTask.dispatchTask(data)
	local ret, result
	local cmd = data[1]
	
	--取高四位确定哪个应用
	local high = string.sub(cmd, 1, 4)
	
	
	if high == "9000" then
		--全局命令
		ret, result = CommonOps.doTask(data)
	elseif high == "0000" then
		--微信应用
		ret, result = WechatOps.doTask(data) 
	elseif high == "1000" then
		--facebook应用
		ret, result = FacebookOps.doTask(data) 
	end
	
	return ret, result
end

function DoTask.doTask(tb_data, test_account, index)
  --是否开启录屏
  local screen_record_switch = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'screen_record_switch', 'false')
  if screen_record_switch == 'true' then
    stopScreenRecord()
    startScreenRecord('/sdcard/record.mp4')
  end
  
	if SharedDic.get('test_wx_switch') == 'true' or SharedDic.get('test_fb_switch') == 'true' then
		local jsdata
		if SharedDic.get('test_fb_switch') == 'true' then
			jsdata = AutomatorApi:readFile(getPath().."/tmp/test_fb.json")
		else
			jsdata = AutomatorApi:readFile(getPath().."/tmp/test_wx.json")
		end
		tb_data = json.decode(jsdata)
		SharedDic.set('task_id', tb_data["task_id"])
		SharedDic.set('task_type', tb_data["task_type"])
	end
	
	local task_id = tb_data["task_id"]
	local task_type = tb_data["task_type"]
	local guid = AutomatorApi:readFile(getPath().."/uuid.txt")
	local var
	local ret
	local str_post
	local tb_task = {}
	local msg_tmp = ""
	local msg = ""
	local acc_info

	tb_task = tb_data["cmd"]
	
	if SharedDic.get('test_wx_switch') == 'true' then
		tb_task[1]["op"][2] = test_account[index][3]
		tb_task[2]["op"][2] = test_account[index][1]
		tb_task[2]["op"][3] = test_account[index][2]
		AutomatorApi:toast(tb_task[1]["op"][2].."\n"..tb_task[2]["op"][2].."\n"..tb_task[2]["op"][3])
    mSleep(2000)
	end
	
	if SharedDic.get('test_fb_switch') == 'true' then
		local imei = getRandomImei()
		AutomatorApi:toast(imei)
    mSleep(2000)
		tb_task[1]["op"][2] = imei
		if task_type == "fb_reg_account" then
      test_account[index] = string.gsub(test_account[index], "\n", "")
      test_account[index] = string.gsub(test_account[index], "\r", "")
      AutomatorApi:logAppend("FbRegisterTotal", tb_task[1]["op"][2]..'\t'..test_account[index])
      AutomatorApi:toast(test_account[index])
      mSleep(2000)
      acc_info = string.split(test_account[index], '\t')
      tb_task[2]["op"][2] = acc_info[4]
      tb_task[2]["op"][3] = acc_info[2]
      tb_task[2]["op"][4] = acc_info[3]
      tb_task[2]["op"][5] = acc_info[6]
      tb_task[2]["op"][6] = acc_info[5]
      
      local birth = string.split(acc_info[7], '-')
      tb_task[2]["op"][7] = birth[1]
      tb_task[2]["op"][8] = birth[2]
      tb_task[2]["op"][9] = birth[3]
    end 
  end
	
  
	local check_times = 10
	local network_avaliable = true
--  if SharedDic.get('test_fb_switch') ~= 'true' then
--    while check_times > 0 do
--      network_avaliable = check_network_avaliable()
--      if network_avaliable == true then
--        break
--      end
--      AutomatorApi:mSleep(3000)
--      check_times = check_times - 1
--    end
--  end
  
	if network_avaliable == false then
		AutomatorApi:log("main", "Network is not avaliable.")
		ret = RET_NORMAL_FAILED
		msg = "Network is not avaliable."
	else
		for var = 1, #tb_task do
			local iRet, sRet = pcall(function()	
				ret, msg_tmp = DoTask.dispatchTask(tb_task[var]["op"])
			end)
			if iRet == false then
				msg = msg..'|'..'Exception error.'..sRet
				AutomatorApi:logAppend("Exception", "DoTask.doTask: "..msg)
        
        --facebook测试异常记录
        if SharedDic.get('test_fb_switch') == 'true' then
          local ip = AutomatorApi:executeShellCommand("curl -s whatismyip.akamai.com")
          AutomatorApi:toast(sRet)
          mSleep(2000)
          AutomatorApi:logAppend("FbRegisterFail", ip..'\t'..tb_task[1]["op"][2]..'\t'..tb_task[2]["op"][2]..'\t'..tb_task[2]["op"][6]..'\t'..msg)
        end
				return
			end
			
      msg_tmp = msg_tmp or ""
			msg = msg..'|'..msg_tmp
			AutomatorApi:toast(msg_tmp)
      mSleep(2000)
			local task_detail = json.encode(tb_task[var]["op"])
			AutomatorApi:logAppend("task_detail", task_detail.."\t"..tostring(ret).."|"..msg_tmp)
			
			if ret ~= RET_SUCCEEDED then
				if tb_task[var]["can_ignore"] == nil or tb_task[var]["can_ignore"] == 0 then
					break
				end
			end
		end
	end

  --停止录屏
  if screen_record_switch == 'true' then
    stopScreenRecord()
  end

	local pic_name = getPath().."/tmp/snapshot.png"
	local result 

	AutomatorApi:executeShellCommand("mkdir /sdcard/GodHand/log/screenshot")
	--保存当前页面截图
	AutomatorApi:takeScreenshot(pic_name, 50)
	if ret ~= RET_SUCCEEDED then
		local usrId = SharedDic.get('usrId')
		if usrId ~= nil then
			AutomatorApi:resizeImage(pic_name, getPath().."/log/screenshot/"..usrId.."_.jpg", 360, 640)
		end
	end
	-- AutomatorApi:convert2GrayImage(pic_name)
	AutomatorApi:resizeImage(pic_name, pic_name, 360, 640)
	result = {
		["status"]=ret,
		["message"]=msg,
		["snapshot_path"]=AutomatorApi:base64EncodeFile(pic_name)
	}

	result["task_id"] = task_id
	result["guid"] = guid
	result["registAccount"] = WechatOps.strCurUsr
	result["uin"] = SharedDic.get('uin')
  local ip = nil
  local getip_times = 5
  while ip == nil and getip_times > 0 do
    ip = AutomatorApi:executeShellCommand("curl -s whatismyip.akamai.com")
    getip_times  =getip_times - 1
    mSleep(3000)
  end
  result["out_ip"] = ip


	str_post = json.encode(result)
	if SharedDic.get('test_wx_switch') == 'false' and SharedDic.get('test_fb_switch') == 'false' then
		TaskInterface.postTaskResult(result)
    if screen_record_switch == 'true' then
      local spath = getPath()..'/log/screenrecord/'..os.date("%Y-%m-%d")..'/'
      AutomatorApi:executeShellCommand('mkdir -p '..spath)
      --保留最近三天的录屏
      AutomatorApi:executeShellCommand('busybox ls --color=nerver -c '..getPath()..'/log/screenrecord/'..'|busybox tail +4|busybox xargs rm -fr')
      AutomatorApi:executeShellCommand('mv /sdcard/record.mp4 '..spath..task_id..'.mp4')
    end
	else  --下面是本地测试用
    if SharedDic.get('test_fb_switch') == 'true' then
--      if screen_record_switch == 'true' then
--        local spath = getPath()..'/log/screenrecord/'..os.date("%Y-%m-%d")..'/fb/'
--        AutomatorApi:executeShellCommand('mkdir -p '..spath)
--        --保留最近三天的录屏
--        AutomatorApi:executeShellCommand('busybox ls --color=nerver -c '..getPath()..'/log/screenrecord/|busybox tail +4|busybox xargs rm -fr')
--        AutomatorApi:executeShellCommand('mv /sdcard/record.mp4 '..spath..tb_task[2]["op"][2]..'.mp4')
--      end
      
      if result['status'] == RET_SUCCEEDED then
        AutomatorApi:writeFile(getPath().."/tmp/cur_fb_email.txt", tb_task[2]["op"][2])
        AutomatorApi:logAppend("FbRegisterSuc", ip..'\t'..acc_info[1]..'\t'..tb_task[1]["op"][2]..'\t'..tb_task[2]["op"][2]..'\t'..tb_task[2]["op"][6])
--        CommonOps.sleep(3600000)
      else
        if screen_record_switch == 'true' then
          local spath = getPath()..'/log/screenrecord/'..os.date("%Y-%m-%d")..'/fb/'
          AutomatorApi:executeShellCommand('mkdir -p '..spath)
          --保留最近三天的录屏
          AutomatorApi:executeShellCommand('busybox ls --color=nerver -c '..getPath()..'/log/screenrecord/|busybox tail +4|busybox xargs rm -fr')
          AutomatorApi:executeShellCommand('mv /sdcard/record.mp4 '..spath..tb_task[2]["op"][2]..'.mp4')
        end
        
        AutomatorApi:logAppend("FbRegisterFail", ip..'\t'..acc_info[1]..'\t'..tb_task[1]["op"][2]..'\t'..tb_task[2]["op"][2]..'\t'..tb_task[2]["op"][6]..'\t'..msg)
        AutomatorApi:resizeImage(pic_name, getPath().."/log/screenshot/"..tb_task[2]["op"][2].."_.jpg", 360, 640)
      end
    end
		AutomatorApi:logAppend("test_result", "DoTask.doTask: "..msg)
	end
  
  return result['status']
end