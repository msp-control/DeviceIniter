package.cpath = "/data/data/com.rzx.godhandmator/app_libs/?.so" --;/sdcard/GodHand/plugin/?.so
package.path = package.path..";/sdcard/GodHand/plugin/?.lua"

require 'import'
import 'com.rzx.godhandmator.AutomatorApi'

local local_ip = "127.0.0.1"
local udp_port = 12580
local socket = require("socket")
local udpsocket = socket.udp4()

--sleep
function mSleep(ms)
	ms = tonumber(ms)
	while ms > 0 do
		if ms >= 1000 then
			AutomatorApi:mSleep(1000)
		else
			AutomatorApi:mSleep(ms)
		end
		if udpsocket ~= nil then
			udpsocket:sendto("alive", local_ip, udp_port)
		end
		ms = ms - 1000
	end
end

--获取滚动条控件
function getScrollable()
	local uiSelector,uiScrollable
	uiSelector = luajava.newInstance('android.support.test.uiautomator.UiSelector')
	uiSelector = uiSelector:scrollable(true)
	if uiSelector ~= nil then
		uiScrollable = luajava.newInstance('android.support.test.uiautomator.UiScrollable', uiSelector)
	end
	return uiScrollable
end

--返回程序HOME目录
function getPath()
	return '/mnt/sdcard/GodHand'
end

--字符串分割
function string.split(str, delimiter)
	if str==nil or str=='' or delimiter==nil then
		return nil
	end
	
    local result = {}
    for match in (str..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match)
    end
    return result
end

--找色
function findMultiColorInRegionFuzzy(beginColor, otherColors, sim, x1, y1, x2, y2)
	local result
	result = AutomatorApi:findMultiColorInRegionFuzzy(beginColor, otherColors, sim, x1, y1, x2, y2)
	result = string.split(result, ',')
	return tonumber(result[1]),tonumber(result[2])
end

--规定时间内完成找色
function findMultiColorInRegionFuzzyInTime(p1,p2,p3,p4,p5,p6,p7,p8)
	local var
	local count
	local x,y
	count = p8/500
	if count == 0 then count = 1 end
	for var = 1, count do
		AutomatorApi:mSleep(500)
		x,y = findMultiColorInRegionFuzzy( p1,p2,p3,p4,p5,p6,p7)
		if x ~= -1 and y ~= -1 then
			return x, y
		end
	end
	return x,y
end

--计算文件长度
function lengthOfFile(filename)
	if AutomatorApi:fileExists(filename) == false then
		return -1
	end
	
	local fh = io.open(filename, "rb")
	local len = fh:seek("end")
	fh:close()
	return len
end

--获取目录下文件列表
function getFileList(dir)
	local file_list = {}
	local cmd = "ls "..dir.." > /sdcard/GodHand/tmp/file_list"
	AutomatorApi:executeShellCommand(cmd)
	local s = io.open("/sdcard/GodHand/tmp/file_list")
	if s ~= nil then
		local line
		for line in s:lines() do 
			table.insert(file_list, dir.."/"..line)
		end
		s:close()
	end
	return file_list
end

--读取文件每一行到table
function readFileTable(filename)
	local file_table = {}
	local s = io.open(filename)
	if s ~= nil then
		local line
		for line in s:lines() do 
			table.insert(file_table, line)
		end
		s:close()
	end
	return file_table
end

--获取最上层Activity
function getTopActivity()
	local iRet, sRet = pcall(function()
		local result = AutomatorApi:executeShellCommand("dumpsys activity top")
		return string.match(result,"ACTIVITY ([^ ]+)")
	end)
	if iRet == true then
		return sRet
	else
		return ""
	end
end

--等待转到新窗口
function waitNewWindow(old, time_out)
	local top_activity = ""
	while time_out > 0 do
		top_activity = getTopActivity()
		if top_activity ~= old then
			return true
		end
		time_out = time_out - 500
		AutomatorApi:mSleep(450)
	end
	
	return false
end

--检查网络是否可用
function check_network_avaliable()
	-- local ret = AutomatorApi:executeShellCommand("busybox ping -c 2 114.114.114.114 | |busybox grep received |busybox awk -F',' '{print $2}'| busybox awk '{print $1}'")
	-- if ret == '1' or ret == '2' then
		-- return true
	-- end
	local ret = AutomatorApi:executeShellCommand("curl -I -k -s -m 3 --connect-timeout 3 https://www.baidu.com -w %{http_code} | busybox tail -n1")
	if ret ~= nil and ret == "200" then
		return true
	end
	return false
end

----添加通讯录联系人
----文件内容格式(换行符必须需是\n，没有回车符\r):
----		姓名,号码
----		姓名,号码
--function addContacts(filename)
--	AutomatorApi:executeShellCommand("am startservice -n com.rzx.appsmator/.services.ActionService --es cmd addContacts --es file '"..filename.."'")
--end

----清空通讯录联系人
--function clearContacts()
--	AutomatorApi:executeShellCommand("am startservice -n com.rzx.appsmator/.services.ActionService --es cmd clearContacts")
--end

--开启飞行模式
function openAirplaneMode()
	AutomatorApi:executeShellCommand("settings put global airplane_mode_on 1")
	AutomatorApi:executeShellCommand("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true")
	AutomatorApi:mSleep(2000)
end

--关闭飞行模式
function closeAirplaneMode()
	AutomatorApi:executeShellCommand("settings put global airplane_mode_on 0")
	AutomatorApi:executeShellCommand("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false")
	if AutomatorApi:waitNewWindowByTextEqual('确定', 10000) then
		AutomatorApi:clickByTextEqual('确定', 0)
	end
end

--启动录屏
function startScreenRecord(path, size, bit_rate, time)
  local _size = size or '1280x720'
  local _bit_rate = bit_rate or '800000'
  local _time = time or '3600'
  
  local cmd = 'screenrecord_no_time_limit'
    ..' --size '.._size
    ..' --bit-rate '.._bit_rate
    ..' --time-limit '.._time
    ..' '..path..' > /dev/null 2>&1'
  AutomatorApi:executeShellCommandNoWait(cmd)
end

--停止录屏
function stopScreenRecord()
  AutomatorApi:executeShellCommand("busybox ps -ef|grep screenrecord_no_time|grep -v 'grep'|busybox awk '{print $1}'|busybox xargs kill -2")
end

--删除编辑框字符
function delText(times)
  for var=1,times do
    AutomatorApi:pressKeyCode(67)
  end
end

--获取facebook邮箱注册码
function getFbRegEmailCode(usr, pwd, timeout)
  local emailMap = {
    ['dustin2000.tk'] = {'imap.dustin2000.tk', '143'}
  }
  
  local ret, res
  local tbl_usr = string.split(usr, '@')
  if not tbl_usr or #tbl_usr ~= 2 then
    return false, 'Get email code failed. Error format email.'
  end
  
  if tbl_usr[2] == 'gmx.com' then
    local code = AutomatorApi:executeShellCommand("sh /sdcard/GodHand/plugin/get_gmx_code.sh "
      ..usr.." "..pwd.." "..timeout.." mark 2> "..getPath().."/log/get_mx_code.log")
    if code == nil or code == "" then
      return false, "Get email code error."
    end
    
    local vcode = string.match(code, '%d%d%d%d%d')
    if vcode == nil then
      return false, "Get email code error."..code
    else
      return true, code
    end
  end
  
  local mailServer, mailPort
  if emailMap[tbl_usr[2]] == nil then
    mailServer = 'pop3.'..tbl_usr[2]
    mailPort = '110'
  else
    mailServer = emailMap[tbl_usr[2]][1]
    mailPort = emailMap[tbl_usr[2]][2]
  end

	local iRet,sRet
	if string.find(mailServer, 'imap.') ~= nil then
		iRet,sRet = pcall( function() res = AutomatorApi:getFbEmailVCodeFromImap(mailServer, mailPort, usr, pwd, timeout) end )
	else
		iRet,sRet = pcall( function() res = AutomatorApi:getFbEmailVCodeFromPop3(usr, pwd, timeout) end )
	end
	
  if not iRet then
    ret = false
    res = sRet or 'Get email code failed.'..(sRet or 'Unknown error.')
  else
    ret = res and true or false
    res = res or 'Get email code failed. Read timeout.'
  end
  return ret,res
end

--luhn 校验算法获取校验位
function getLuhnCheckDigit(str)
  local ret
  local sum = 0
  local var = #str
  local tmp = 1
  while var>0 do
    local tmp_n = tonumber(string.sub(str, var, var))
    if tmp%2 == 0 then
      sum = sum + tmp_n
    else
      sum = sum + math.modf(tmp_n*2/10) + tmp_n*2%10
    end
    var = var - 1
    tmp = tmp + 1
  end
  
  if sum%10 == 0 then
    ret = '0'
  else
    ret = tostring(10 - sum%10)
	end
  return ret
end

--获取随机IMEI
function getRandomImei()
    local pre8 = '86698002'
    local mid6 = ''
    local last1 = ''
    local dig = '0123456789'
    
	math.randomseed(os.time())
    for var=0,5 do
		local rand_i = math.random(10)
        mid6 = mid6..string.sub(dig, rand_i, rand_i)
	end
        
  local pre14 = pre8..mid6
  last1 = getLuhnCheckDigit(pre14) 

  return pre8..mid6..last1
end

function getRandomPhone()
	local pre3 = {'133', '151', '188', '187', '155', '132'}
	math.randomseed(os.time())
	local last8 = math.random(10000000, 99999999)
	
	return pre3[math.random(1, #pre3)]..tostring(last8)
end
