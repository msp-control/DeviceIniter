require 'GHLib'
require 'ProtocolDefine'
require 'CommonOps'
import 'android.support.test.uiautomator.Until'
import 'android.support.test.uiautomator.UiSelector'
import 'android.support.test.uiautomator.By'
import 'com.rzx.godhandmator.ParamsGenerator'

FacebookOps = {}
FacebookOps = {strCurUsr = ""}

local t_monthEN = {
  ['1'] = 'Jan',
  ['2'] = 'Feb',
  ['3'] = 'Mar',
  ['4'] = 'Apr',
  ['5'] = 'May',
  ['6'] = 'Jun',
  ['7'] = 'Jul',
  ['8'] = 'Aug',
  ['9'] = 'Sep',
  ['10'] = 'Oct',
  ['11'] = 'Nov',
  ['12'] = 'Dec'
}

local t_textsZH = {
  ['register_btn_create_new_fb_account'] = '新建 Facebook 帐户',
  ['register_btn_retry'] = '重试',
  ['register_btn_next'] = '继续',
  ['register_btn_signup_with_eamil'] = '用邮箱注册',
  ['register_btn_signup'] = '注册',
  ['register_btn_try_again'] = '再试一次',
  ['register_btn_skip'] = '跳过',
  ['register_btn_done'] = '完成',
  ['register_btn_confirm'] = '确认',
  ['register_title_create_account'] = '创建帐户',
  ['register_title_mobile_number'] = '手机号',
  ['register_title_name'] = '姓名',
  ['register_title_birthday'] = '生日',
  ['register_title_gender'] = '性别',
  ['register_title_password'] = '密码',
  ['register_title_privacy'] = '条款和隐私',
  ['register_title_add_your_photo'] = "添加头像",
  ['register_title_add_friends'] = '加好友',
  ['register_title_account_confirm'] = "帐户验证",
  ['register_ck_male'] = '男',
  ['register_ck_female'] = '女',
  ['register_txt_existing_account_err'] = "已有一个帐户与此邮箱关联。",
  ['register_txt_confirm_identity'] = "确认身份",
  ['mainwnd_tab_news_feed'] = '动态消息',
  ['mainwnd_tab_more'] = '更多',
  ['mainwnd_more_find_friends'] = '搜索好友',
  ['findfriendswnd_title'] = '搜索好友',
  ['findfriendswnd_tab_contact'] = '通讯录',
  ['findfriendswnd_btn_get_started'] = '开始',
  ['findfriendswnd_btn_add_friend'] = "加为好友",
  ['findfriendswnd_txt_contacts'] = "联系人"
}

local t_textsEN = {
  ['register_btn_create_new_fb_account'] = 'Create New Facebook Account',
  ['register_btn_retry'] = 'Retry',
  ['register_btn_next'] = 'Next',
  ['register_btn_signup_with_eamil'] = 'Sign Up With Email Address',
  ['register_btn_signup'] = 'Sign Up',
  ['register_btn_try_again'] = 'Try Again',
  ['register_btn_skip'] = 'Skip',
  ['register_btn_done'] = 'Done',
  ['register_btn_confirm'] = 'Confirm',
  ['register_title_create_account'] = 'Create Account',
  ['register_title_mobile_number'] = 'Mobile Number',
  ['register_title_name'] = 'Name',
  ['register_title_birthday'] = 'Birthday',
  ['register_title_gender'] = 'Gender',
  ['register_title_password'] = 'Password',
  ['register_title_privacy'] = 'Terms & Privacy',
  ['register_title_add_your_photo'] = 'Add Your Photo',
  ['register_title_add_friends'] = 'Add Friends',
  ['register_title_account_confirm'] = 'Account Confirmation',
  ['register_ck_male'] = 'Male',
  ['register_ck_female'] = 'Female',
  ['register_txt_existing_account_err'] = 'There is an existing account associated with this email.',
  ['register_txt_confirm_identity'] = 'Confirm Your Identity',
  ['mainwnd_tab_news_feed'] = 'News Feed',
  ['mainwnd_tab_more'] = 'More',
  ['mainwnd_more_find_friends'] = 'Find Friends',
  ['findfriendswnd_title'] = 'Find Friends',
  ['findfriendswnd_tab_contact'] = 'CONTACTS',
  ['findfriendswnd_btn_get_started'] = 'Get Started',
  ['findfriendswnd_btn_add_friend'] = "ADD FRIEND",
  ['findfriendswnd_txt_contacts'] = "CONTACTS"
}

--import 'java.util.Locale'
--local lang = Locale:getDefault():getLanguage()
--local t_texts = lang == 'en' and t_textsEN or t_textsZH

local lang_flag = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'com.facebook.katana.LANG', 'en_US')
local t_texts = lang_flag == 'en_US' and t_textsEN or t_textsZH

--注册
function FacebookOps.register(country_name, phone, email, first_name, last_name, gender, password, birth_year, birth_month, birth_day)
	local ret, msg
	ret = RET_SUCCEEDED
	msg = email..' Register succeeded.'
  
	AutomatorApi:executeShellCommand("am force-stop com.facebook.katana")
  AutomatorApi:executeShellCommand("rm -fr /data/data/com.facebook.katana/*")
  
  local airplaneSwitch = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'airplane_switch', 'true')
  if airplaneSwitch == 'true' then
    AutomatorApi:toast("开启飞行模式")
    openAirplaneMode()
    AutomatorApi:toast("关闭飞行模式")
    closeAirplaneMode()
  end
  
  --生成hook信息文件
  ParamsGenerator:generateParams(context, SharedDic.get('imei'))
  ret, msg = CommonOps.checkHook()
  --if ret ~= RET_SUCCEEDED then
    --return RET_NORMAL_FAILED, email..' Register failed. '..msg
  --end
	
	AutomatorApi:executeShellCommand("am start -n com.facebook.katana/.LoginActivity")
	ret = AutomatorApi:waitNewWindowByTextEqual(t_texts['register_btn_create_new_fb_account'], 100000)
	if ret == false then
		return RET_NORMAL_FAILED, email..' Register failed. Can not open com.facebook.katana/.LoginActivity'
	end
	
	AutomatorApi:clickByTextEqual(t_texts['register_btn_create_new_fb_account'], 0)
	repeat
		local time_out = 135000
		local retry_cnt = 2
		local object
		while time_out > 0 do
			object = nil
			if retry_cnt > 0 then
				object = uiDevice:findObject(By:text(t_texts['register_btn_retry']))
				if object ~= nil then
					AutomatorApi:toast(t_texts['register_btn_retry'])
					AutomatorApi:mSleep(1000)
					AutomatorApi:clickByTextEqual(t_texts['register_btn_retry'], 0)
					retry_cnt = retry_cnt - 1
				end
			end
			
			object = uiDevice:findObject(By:text(t_texts['register_title_create_account']))
			if object ~= nil then
				break
			end
			time_out = time_out - 1000
			mSleep(1000)
		end
		if time_out <= 0 then
			ret = RET_NORMAL_FAILED
			msg = 'Register failed. Open com.facebook.registration.activity.AccountRegistrationActivity timeout.'
			break
		end
		
		AutomatorApi:clickByTextEqual(t_texts['register_btn_next'], 0)
		AutomatorApi:mSleep(500)
		
		time_out = 15000
		local is_name = false
		local is_input_phone = false
		local is_birth = false
		local is_sex = false
		local is_password = false
		local is_confirm = false
		while time_out > 0 do
			repeat
				if is_name == false and AutomatorApi:waitNewWindowByTextEqual(t_texts['register_title_name'], 1) then
					is_name = true
					AutomatorApi:clickByResId('com.facebook.katana:id/first_name_input', 0)
          AutomatorApi:inputText(first_name)
          AutomatorApi:mSleep(1500)
					AutomatorApi:clickByResId('com.facebook.katana:id/last_name_input', 0)
          AutomatorApi:inputText(last_name)
          AutomatorApi:mSleep(1500)    
					AutomatorApi:clickByTextEqual(t_texts['register_btn_next'], 0)
					AutomatorApi:mSleep(500)
					break
				elseif is_input_phone == false and AutomatorApi:waitNewWindowByTextEqual(t_texts['register_title_mobile_number'], 1) then
					is_input_phone = true
					AutomatorApi:clickByTextEqual(t_texts['register_btn_signup_with_eamil'], 0)
					AutomatorApi:mSleep(500)
					AutomatorApi:setTextByClass('android.widget.EditText', email, 0)
					AutomatorApi:mSleep(200)
					AutomatorApi:clickByTextEqual(t_texts['register_btn_next'], 0)
					break
				elseif is_birth == false and AutomatorApi:waitNewWindowByTextEqual(t_texts['register_title_birthday'], 1) then
					is_birth = true
          if t_texts == t_textsZH then
            AutomatorApi:setTextByClass('android.widget.EditText', birth_year, 0)
            AutomatorApi:setTextByClass('android.widget.EditText', birth_month, 1)
            AutomatorApi:setTextByClass('android.widget.EditText', birth_day, 2)
          elseif t_texts == t_textsEN then
            AutomatorApi:setTextByClass('android.widget.EditText', t_monthEN[birth_month], 0)
            AutomatorApi:setTextByClass('android.widget.EditText', birth_day, 1)
            AutomatorApi:setTextByClass('android.widget.EditText', birth_year, 2)
          end
					AutomatorApi:mSleep(200)
					AutomatorApi:clickByTextEqual(t_texts['register_btn_next'], 0)
					AutomatorApi:mSleep(500)
					break
				elseif is_sex == false and AutomatorApi:waitNewWindowByTextEqual(t_texts['register_title_gender'], 1) then
					is_sex = true
					if gender == "male" or gender == "Male" then
						AutomatorApi:clickByTextEqual(t_texts['register_ck_male'], 0)
					else
						AutomatorApi:clickByTextEqual(t_texts['register_ck_female'], 0)
					end
					AutomatorApi:mSleep(200)
					AutomatorApi:clickByTextEqual(t_texts['register_btn_next'], 0)
					AutomatorApi:mSleep(500)
					break
				elseif is_password == false and AutomatorApi:waitNewWindowByTextEqual(t_texts['register_title_password'], 1) then
					is_password = true
					AutomatorApi:setTextByClass('android.widget.EditText', password, 0)
					AutomatorApi:mSleep(200)
					AutomatorApi:clickByTextEqual(t_texts['register_btn_next'], 0)
					AutomatorApi:mSleep(500)
					break
        elseif is_password and uiDevice:findObject(By:text(t_texts['register_btn_next'])) then
          object = uiDevice:findObject(By:text(t_texts['register_btn_next']))
          if object then
            object:click()
          end
          break
				elseif is_confirm == false and AutomatorApi:waitNewWindowByTextEqual(t_texts['register_title_privacy'], 1) then
					is_confirm = true
					-- if g_upload_contact_flag then
						-- AutomatorApi:clickByTextEqual('注册', 0)
					-- else
--						AutomatorApi:clickByTextEqual('注册但不上传通讯录', 0)
            AutomatorApi:clickByTextEqual(t_texts['register_btn_signup'], 0)
					-- end
					AutomatorApi:mSleep(500)
					ret,msg = FacebookOps.onRegisterBtnClicked(country_name, phone, email, first_name, last_name, gender, password, birth_year, birth_month, birth_day)
          return ret, msg
				else
					time_out = time_out -1000
					AutomatorApi:mSleep(1000)
				end
			until (true)
		end
		
		if time_out <= 0 then
			ret = RET_NORMAL_FAILED
			msg = "Register failed. Unknown error."
			break
		end
	until (true)
	
	return ret, msg
end

--点击注册按钮
function FacebookOps.onRegisterBtnClicked(country_name, phone, email, first_name, last_name, gender, password, birth_year, birth_month, birth_day)
	local time_out = 40000
	local object
  local try_airplane_times = 0
	while time_out > 0 do
		repeat
      ------------
			object = uiDevice:findObject(By:text(t_texts['register_btn_next']))
			if object ~= nil then
				object:click()
				break
			end
      
      ------------
			object = uiDevice:findObject(By:text(t_texts['register_btn_signup']))
			if object ~= nil then
				object:click()
				break
			end
      
			------------
			object = uiDevice:findObject(By:text(t_texts['register_txt_confirm_identity'])) 
        or uiDevice:findObject(By:desc(t_texts['register_txt_confirm_identity']))
			if object ~= nil then
				return RET_NEED_SMS_VERIFY_FAILED, email..' Register failed. Need Authentication'
			end
			
			------------
			object = uiDevice:findObject(By:text(t_texts['register_btn_try_again']))
        or uiDevice:findObject(By:desc(t_texts['register_btn_try_again']))
			if object ~= nil then
        local airplaneSwitch = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'airplane_switch', 'true')
        if airplaneSwitch == 'true' and try_airplane_times > 1 then
          try_airplane_times = try_airplane_times + 1
          AutomatorApi:toast("开启飞行模式")
          openAirplaneMode()
          AutomatorApi:toast("关闭飞行模式")
          closeAirplaneMode()
        end
        if AutomatorApi:waitNewWindowByTextEqual(t_texts['register_btn_try_again'], 30000) then
          AutomatorApi:clickByTextEqual(t_texts['register_btn_try_again'], 0)
          break
        else
          return RET_NORMAL_FAILED, email..' Register failed. Network is not avaliable.'
        end
			end
			
			------------
			object = uiDevice:findObject(By:text(t_texts['register_title_add_your_photo']))
        or uiDevice:findObject(By:desc(t_texts['register_title_add_your_photo']))
			if object ~= nil then
				AutomatorApi:clickByTextEqual(t_texts['register_btn_skip'], 0)
				AutomatorApi:mSleep(500)
				break
			end
      
      ------------
			object = uiDevice:findObject(By:text(t_texts['register_txt_existing_account_err'])) 
        or uiDevice:findObject(By:desc(t_texts['register_txt_existing_account_err']))
      if object ~= nil then
				return RET_NORMAL_FAILED, email..' Register failed. Already registered.'
			end
      
      ------------
      object = uiDevice:findObject(By:text('NOT NOW'))
        or uiDevice:findObject(By:desc('NOT NOW'))
      if object ~= nil then
        object:click()
        break
      end
			
      ------------
      if FacebookOps.isMainActivity() then
        return RET_SUCCEEDED, email..' Register succeeded, but an already exist account.'
      end
      
			------------只有不上传通讯录注册才会由此提示
--			object = uiDevice:findObject(By:text("寻找朋友")) or uiDevice:findObject(By:desc("寻找朋友"))
--			if object ~= nil then
--        object = uiDevice:findObject(By:text(t_texts['register_btn_skip'])) 
--          or uiDevice:findObject(By:desc(t_texts['register_btn_skip']))
--        if object ~= nil then
--          AutomatorApi:clickByTextEqual(t_texts['register_btn_skip'], 0)
--          AutomatorApi:mSleep(1000)
--          object = uiDevice:findObject(By:text("寻找朋友")) or uiDevice:findObject(By:desc("寻找朋友"))
--          if object ~= nil then
--            AutomatorApi:clickByTextEqual(t_texts['register_btn_skip'], 0)
--          end
--        else
--          object = uiDevice:findObject(By:text("上传通讯录")) or uiDevice:findObject(By:desc("上传通讯录"))
--          if object ~= nil then
--            object:click()
--          end
--        end
--				break
--			end
      
      ------------
			object = uiDevice:findObject(By:text(t_texts['register_title_add_friends']))
      if object ~= nil then
        object = uiDevice:findObject(By:text(t_texts['register_btn_done']))
        if object then
          object:click()
          mSleep(1000)
          break
        end
        
        object = uiDevice:findObject(By:text(t_texts['register_btn_skip']))
        if object then
          object:click()
          mSleep(1000)
        end
        break
      end 
      
      ------------英文版“添加好友”和“加好友”都是“Add Friends”,以下只针对中文版判断
			object = uiDevice:findObject(By:text("添加好友"))
      if object ~= nil then
        if AutomatorApi:waitNewWindowByTextEqual('跳过', 3000) then
          AutomatorApi:clickByTextEqual('跳过', 0)
          AutomatorApi:mSleep(1000)
        end
        break
      end
			
			------------
			object = uiDevice:findObject(By:text(t_texts['register_title_account_confirm']))
        or uiDevice:findObject(By:desc(t_texts['register_title_account_confirm']))
			if object ~= nil then
				local ret, res = getFbRegEmailCode(email, password, 100)
        if not ret then
          if string.find(res, 'jsessionid null') or 
            string.find(res, 'ott null') then
              return RET_INVALID_EMAIL, email.." Register failed. "..res
          end
          return RET_NORMAL_FAILED, email.." Register failed. "..res
        end
        
				AutomatorApi:setTextByClass('android.widget.EditText', res, 0)
				AutomatorApi:clickByTextEqual(t_texts['register_btn_confirm'], 0)
        
        local timeout = 15000
        while timeout > 0 do
          if FacebookOps.isMainActivity() then
            break
          end
          
          object = uiDevice:findObject(By:res('com.facebook.katana:id/error_text'))
          if object ~= nil then
            return RET_NORMAL_FAILED, email..' Register failed, Get email code error. Get wrong code.'
          end
          
          timeout = timeout - 1000
          mSleep(1000)
        end
				return RET_SUCCEEDED, email.." Register succeeded."
			end
		until true
		time_out = time_out -1000
		AutomatorApi:mSleep(1000)
	end
	
	return RET_NORMAL_FAILED, email..' Register failed. Unknown error.'
end

function FacebookOps.isMainActivity()
  local object = uiDevice:findObject(By:desc(t_texts['mainwnd_tab_news_feed']))
    
  return object ~= nil
end

--进入个人主页
function FacebookOps.goHomePage()
  local object
  local timeout = 20000
  while timeout > 0 do
    AutomatorApi:executeShellCommand("am start --activity-no-history -W -n com.facebook.katana/.LoginActivity")
    mSleep(3000)  
    if FacebookOps.isMainActivity() then 
      break 
    end  
    timeout = timeout - 2000
  end

  object = uiDevice:findObject(By:desc("已选中，更多")) 
    or uiDevice:findObject(By:desc(t_texts['mainwnd_tab_more']))
  if object == nil then
    return RET_NORMAL_FAILED, "Go homepage failed. Can't find tab 更多"
  end
  object:click()
  
  flag_r = AutomatorApi:waitNewWindowByDescEqual(t_texts['mainwnd_more_find_friends'], 10000)
	if flag_r == false then
		return RET_NORMAL_FAILED, "Go homepage failed. Can't find 搜索好友."
	end
--  object = uiDevice:findObject(By:descContains('查看个人主页'))
--  if object then object:click() end
  AutomatorApi:click(293, 283)
  return RET_SUCCEEDED, ''
end

--进入编辑个人主页, 第二个参数表示点击标签的右方编辑按钮进入
function FacebookOps.goEditProfile(which_info, clickEdit)
  local object
  local timeout
  
  local flag_r, msg = FacebookOps.goHomePage()
  if flag_r ~= RET_SUCCEEDED then
    return RET_NORMAL_FAILED, "Go edit profile failed. "..msg
  end
  
  timeout = 5000
  while timeout > 0 do
    object = uiDevice:findObject(By:text('更新信息')) or
      uiDevice:findObject(By:text('活动日志')) or
      uiDevice:findObject(By:text('添加你的简介'))
    if object ~= nil then break end
    timeout = timeout - 1000
    mSleep(1000)
  end
  
  if timeout <= 0 then
    return RET_NORMAL_FAILED, "Go edit profile failed. Can't find object by text 更新信息 or 活动日志 or 添加你的简介"
  end
  
  local swipe_times = 3
  while swipe_times > 0 do
    object = uiDevice:findObject(By:text('添加你的简介')) or uiDevice:findObject(By:text('编辑详情'))
    if object ~= nil then object:click() break end
    
    object = uiDevice:findObject(By:text('简介'))
    if object ~= nil then
      object:click()
      flag_r = AutomatorApi:waitNewWindowByTextEqual("你的更多资料",  20000)
      if flag_r == false then
        return RET_NORMAL_FAILED, "Go edit profile failed. Can't find object by text 你的更多资料"
      end
      AutomatorApi:clickByTextEqual("你的更多资料",  0)
      break
    end
    
    AutomatorApi:swipe(330,1150,307,212,50)
    mSleep(2000)
    swipe_times = swipe_times - 1
  end
  if object == nil then
    return RET_NORMAL_FAILED, "Go edit profile failed. Can't find object by text 添加你的简介 or 编辑详情"
  end
  
  timeout = 20000
  while timeout > 0 do
--    object = uiDevice:findObject(By:res('com.facebook.katana:id/collection_title_text'))
--    if object ~= nil then break end

    object = uiDevice:findObject(By:text('编辑详细信息'))
    if object ~= nil then break end
    
    object = uiDevice:findObject(By:desc('工作'))
    if object ~= nil then break end
    
    timeout = timeout - 1000
    mSleep(1000)
  end
  if timeout <= 0 then
    return RET_NORMAL_FAILED, "Go edit profile failed. Can't find go to detail info UI."
  end
  
  mSleep(1000)
  AutomatorApi:swipe(317,190,307,470,50)
  object = uiDevice:findObject(By:clazz('android.widget.ProgressBar'))
  while object ~= nil do
    mSleep(1000)
    object = uiDevice:findObject(By:clazz('android.widget.ProgressBar'))
  end
  mSleep(2000)
  
  swipe_times = 3
  while swipe_times > 0 do
    object = uiDevice:findObject(By:text(which_info)) or uiDevice:findObject(By:descContains(which_info))
    if object ~= nil then
      local rect = object:getVisibleBounds()
      if rect.bottom <= 1280 then
        break
      end
    end
    
    AutomatorApi:swipe(330,1150,307,212,50)
    mSleep(2000)
    swipe_times = swipe_times - 1
  end
  if object == nil then
    return RET_NORMAL_FAILED, "Go edit profile failed. Can't find object by text "..which_info
  end
  
  mSleep(2000)
  if clickEdit ~= nil and clickEdit == true then
    local rect = object:getVisibleBounds()
    AutomatorApi:click(660, rect.top+10)
  else
    object:click()
  end
  
  flag_r = AutomatorApi:waitNewWindowByTextEqual("编辑个人主页",  20000)
  if flag_r == false then
    return RET_NORMAL_FAILED, "Go edit profile failed. Can't find object by text 编辑个人主页."
  end
  
  return RET_SUCCEEDED, ''
end

--登录
function FacebookOps.login(usr, password)


end

--设置头像
function FacebookOps.setHead(filename)
  local object
  if AutomatorApi:fileExists(filename) == false then
    return RET_NORMAL_FAILED, "Set head failed. "..filename.." doesn't exist."
  end
  
	AutomatorApi:delImagesMedia()
	AutomatorApi:executeShellCommand("mkdir -p /sdcard/DCIM/Facebook/")
	AutomatorApi:executeShellCommand("cp "..filename.." /sdcard/DCIM/Facebook/")
	AutomatorApi:notifyScanImageFile("/sdcard/DCIM/Facebook/"..string.match(filename, ".*/(.*)"))
	
	local flag_r, msg = FacebookOps.goHomePage()
  if flag_r ~= RET_SUCCEEDED then
    return RET_NORMAL_FAILED, "Set head failed. "..msg
  end
  
	flag_r = AutomatorApi:waitNewWindowByResId("com.facebook.katana:id/pnux_modal_thanks_button",  5000)
	if flag_r then
		AutomatorApi:clickByResId("com.facebook.katana:id/pnux_modal_thanks_button", 0)
	end
  
	flag_r = AutomatorApi:waitNewWindowByResId("com.facebook.katana:id/standard_header_profile_pic",  3000)
	if flag_r == false then
    object = uiDevice:findObject(By:text('添加照片，方便好友找到你。'))
    if object ~= nil then
      object:click()
      flag_r = AutomatorApi:waitNewWindowByResId("com.facebook.katana:id/standard_cover_photo_view",  3000)
      if flag_r == false then
        return RET_NORMAL_FAILED, "Set head failed. Can't go to personal info window."
      else
        AutomatorApi:click(100,457)
      end
    end
	else
    AutomatorApi:clickByResId("com.facebook.katana:id/standard_header_profile_pic", 0)
  end
  
  local timeout = 5000
  while timeout > 0 do
    object = uiDevice:findObject(By:text('选择头像')) or uiDevice:findObject(By:text('上传视频或照片'))
    if object ~= nil then object:click() break end
    timeout = timeout - 1000
    mSleep(1000)
  end
	if object == nil then
		return RET_NORMAL_FAILED, "Set head failed. Can't popup item list when click head."
	end	
	
  timeout = 5000
  while timeout > 0 do
    object = uiDevice:findObject(By:text('图库'))
    if object ~= nil then
      AutomatorApi:click(110,260)
      break
    end
    
    object = uiDevice:findObject(By:text('选择照片'))
    if object ~= nil then
      AutomatorApi:click(348, 362)
      break
    end
    timeout = timeout - 1000
    mSleep(1000)
  end
	if timeout <= 0 then
		return RET_NORMAL_FAILED, "Set head failed. Can't open select photo UI."
	end	
	
	-- AutomatorApi:mSleep(1500)
	-- AutomatorApi:clickByTextEqual("完成", 0)
	
	flag_r = AutomatorApi:waitNewWindowByTextEqual("头像", 5000)
	if flag_r == false then
		return RET_NORMAL_FAILED, "Set head failed. Can't open select photo use UI."
	end	
	flag_r = AutomatorApi:waitNewWindowByTextEqual("使用", 5000)
	if flag_r == false then
		return RET_NORMAL_FAILED, "Set head failed. Can't open select photo use UI."
	end
	AutomatorApi:mSleep(500)
	AutomatorApi:clickByTextEqual("使用", 0)
	AutomatorApi:mSleep(10000)
	
	return RET_SUCCEEDED, "Set head succeeded."
end

--设置封面
function FacebookOps.setCover(filename)
  if AutomatorApi:fileExists(filename) == false then
    return RET_NORMAL_FAILED, "Set cover failed. "..filename.." doesn't exist."
  end
  
	AutomatorApi:delImagesMedia()
	AutomatorApi:executeShellCommand("mkdir -p /sdcard/DCIM/Facebook/")
	AutomatorApi:executeShellCommand("cp "..filename.." /sdcard/DCIM/Facebook/")
	AutomatorApi:notifyScanImageFile("/sdcard/DCIM/Facebook/"..string.match(filename, ".*/(.*)"))
	
	local flag_r, msg = FacebookOps.goHomePage()
  if flag_r ~= RET_SUCCEEDED then
    return RET_NORMAL_FAILED, "Set cover failed. "..msg
  end

	flag_r = AutomatorApi:waitNewWindowByResId("com.facebook.katana:id/pnux_modal_thanks_button",  5000)
	if flag_r then
		AutomatorApi:clickByResId("com.facebook.katana:id/pnux_modal_thanks_button", 0)
	end
	
	flag_r = AutomatorApi:waitNewWindowByResId("com.facebook.katana:id/standard_cover_photo_view",  5000)
	if flag_r == false then
		return RET_NORMAL_FAILED, "Set cover failed. Can't go to personal info window."
	end	
	
	AutomatorApi:mSleep(1000)
	AutomatorApi:click(662, 209)
	AutomatorApi:mSleep(1000)
	flag_r = AutomatorApi:waitNewWindowByResId("com.facebook.katana:id/fbui_popover_list_item_title", 5000)
	if flag_r == false then
		return RET_NORMAL_FAILED, "Set cover failed. Can't popup item list when click cover."
	end	
	AutomatorApi:clickByResId("com.facebook.katana:id/fbui_popover_list_item_title", 0)
	
	flag_r = AutomatorApi:waitNewWindowByTextEqual("图库", 5000)
	if flag_r == false then
		return RET_NORMAL_FAILED, "Set cover failed. Can't open picture library UI."
	end	
	AutomatorApi:mSleep(2000)
	AutomatorApi:click(116, 269)
	flag_r = AutomatorApi:waitNewWindowByTextEqual("保存", 5000)
	if flag_r == false then
		return RET_NORMAL_FAILED, "Set cover failed. Can't open cover save UI."
	end	
	AutomatorApi:clickByTextEqual("保存", 0)
	AutomatorApi:mSleep(10000)
	
	return RET_SUCCEEDED, "Set cover succeeded."
end

--持续上传通讯录
function FacebookOps.addContactFriend(count)
  AutomatorApi:executeShellCommand("am start --activity-no-history -W -n com.facebook.katana/.LoginActivity")
  
  local object
  local timeout = 8000
  while timeout > 0 do
    object = uiDevice:findObject(By:desc("已选中，更多")) 
      or uiDevice:findObject(By:desc(t_texts['mainwnd_tab_more']))
    if object ~= nil then break end
    mSleep(1000)
    timeout = timeout - 1000
  end
  
  if object == nil then
    return RET_NORMAL_FAILED, "Add contact friend failed. Can't find object by desc "..t_texts['mainwnd_tab_more']
  end
  object:click()
--  mSleep(2000)
  
  AutomatorApi:waitNewWindowByDescEqual(t_texts['mainwnd_more_find_friends'],  10000)
  object = uiDevice:findObject(By:desc(t_texts['mainwnd_more_find_friends']))
  if object == nil then
    return RET_NORMAL_FAILED, "Add contact friend failed. Can't find object by desc "..t_texts['mainwnd_more_find_friends']
  end
  object:click()
  mSleep(2000)
  
  flag_r = AutomatorApi:waitNewWindowByTextEqual(t_texts['findfriendswnd_title'],  20000)
	if flag_r == false then
		return RET_NORMAL_FAILED, "Add contact friend failed. Can't find object by text "..t_texts['findfriendswnd_title']
	end
  mSleep(2000)
  
  object = uiDevice:findObject(By:text(t_texts['findfriendswnd_tab_contact']))
  if object == nil then
    return RET_NORMAL_FAILED, "Add contact friend failed. Can't find object by text "..t_texts['findfriendswnd_tab_contact']
  end
  object:click()
  
  if AutomatorApi:waitNewWindowByTextEqual(t_texts['findfriendswnd_btn_get_started'], 6000) then
    AutomatorApi:clickByTextEqual(t_texts['findfriendswnd_btn_get_started'], 0)
  end
  
  timeout = 30000
  while timeout > 0 do
    object = uiDevice:findObject(By:text(t_texts['findfriendswnd_btn_add_friend'])) 
      or uiDevice:findObject(By:text(t_texts['findfriendswnd_txt_contacts']))
    if object ~= nil then break end
    
    AutomatorApi:swipe(317,350,307,932,50)
    mSleep(5000)
    timeout = timeout - 5000
  end
  
  if object == nil then
    return RET_NORMAL_FAILED, "Add contact friend failed. Can't find object by text "..t_texts['findfriendswnd_btn_add_friend']
  end
	
  local addedCount = 0
	local addCount = count
	local swipeCount = 5
	while addCount > 0 and swipeCount >= 0 do
		object = uiDevice:findObject(By:text(t_texts['findfriendswnd_btn_add_friend']))
		if object ~= nil then
			object:click()
      addedCount = addedCount + 1
			AutomatorApi:mSleep(1000)
			addCount = addCount - 1
		else
			AutomatorApi:swipe(719,1279,719,550, 50)
			swipeCount = swipeCount -1
			AutomatorApi:mSleep(3000)
		end
	end
		
	return RET_SUCCEEDED, "Add contact friend succeeded. Added "..addedCount..' friends.'
end

--设置工作信息
function FacebookOps.setWork(work, city)
  local object
  local flag_r, msg = FacebookOps.goEditProfile("添加工作信息")
  if flag_r ~= RET_SUCCEEDED then
    return RET_NORMAL_FAILED, "Set work failed. "..msg
  end
  
  AutomatorApi:swipe(317,190,307,470,50)
  mSleep(5000)
  flag_r = AutomatorApi:waitNewWindowByDescEqual("工作",  20000)
  if flag_r == false then
    return RET_NORMAL_FAILED, "Set work failed. Can't find object by text 工作."
  end
  
  AutomatorApi:click(327,280)
  AutomatorApi:inputText(work)
  AutomatorApi:click(5,192)
  mSleep(1000)
  
  AutomatorApi:click(312,457)
  AutomatorApi:inputText(city)
  mSleep(3000)
  AutomatorApi:click(217,552)
  mSleep(1000)
  AutomatorApi:click(5,192)
  mSleep(1000)
  
  flag_r = AutomatorApi:waitNewWindowByDescEqual("保存", 5000)
  if flag_r == false then
    return RET_NORMAL_FAILED, "Set work failed. Can't find object by description 保存."  
  end
  
  object = uiDevice:findObject(By:desc("保存"))
  if object == nil then
    return RET_NORMAL_FAILED, "Set work failed. Can't find object by text 保存."
  end
  object:click()
  mSleep(5000)
  return RET_SUCCEEDED, "Set work succeeded."
end

--设置大学信息
function FacebookOps.setCollege(college, city)
  local object
  local flag_r, msg = FacebookOps.goEditProfile("添加大学")
  if flag_r ~= RET_SUCCEEDED then
    return RET_NORMAL_FAILED, "Set college failed. "..msg
  end
  
  AutomatorApi:swipe(317,190,307,470,50)
  mSleep(5000)
  flag_r = AutomatorApi:waitNewWindowByDescEqual("学历",  20000)
  if flag_r == false then
    return RET_NORMAL_FAILED, "Set college failed. Can't find object by text 学历."
  end
  
  
  AutomatorApi:click(327,280)
  AutomatorApi:inputText(college)
  AutomatorApi:click(5,151)
  mSleep(1000)
  
  AutomatorApi:click(260,315)
  AutomatorApi:inputText(city)
  mSleep(3000)
  AutomatorApi:click(435,377)
  mSleep(1000)
  AutomatorApi:click(5,151)
  mSleep(1000)
  
  AutomatorApi:swipe(317,470,307,190,20)
  mSleep(2000)
  object = uiDevice:findObject(By:desc("保存"))
  if object == nil then
    return RET_NORMAL_FAILED, "Set college failed. Can't find object by text 保存."
  end
  object:click()
  mSleep(5000)
  return RET_SUCCEEDED, "Set college succeeded."
end

--设置高中
function FacebookOps.setHighSchool(high_school, city)
  local object
  local flag_r, msg = FacebookOps.goEditProfile("添加高中")
  if flag_r ~= RET_SUCCEEDED then
    return RET_NORMAL_FAILED, "Set high school failed. "..msg
  end
  
  AutomatorApi:swipe(317,190,307,470,50)
  mSleep(5000)
  flag_r = AutomatorApi:waitNewWindowByDescEqual("学历",  20000)
  if flag_r == false then
    return RET_NORMAL_FAILED, "Set high school failed. Can't find object by text 学历."
  end
  
  
  AutomatorApi:click(327,280)
  AutomatorApi:inputText(high_school)
  AutomatorApi:click(5,192)
  mSleep(1000)
  
  AutomatorApi:click(347,337)
  AutomatorApi:inputText(city)
  mSleep(3000)
  AutomatorApi:click(337,427)
  mSleep(1000)
  AutomatorApi:click(5,192)
  mSleep(1000)
  
  AutomatorApi:swipe(317,470,307,190,20)
  mSleep(2000)
  object = uiDevice:findObject(By:desc("保存"))
  if object == nil then
    return RET_NORMAL_FAILED, "Set high school failed. Can't find object by text 保存."
  end
  object:click()
  mSleep(5000)
  return RET_SUCCEEDED, "Set high school succeeded."
end

--设置City, town (City必填, Town选填)
function FacebookOps.setPlace(city, town)
  if city == nil or city == '' then
    return RET_NORMAL_FAILED, "Set place failed. City null or empty."
  end
  
  local object
  local flag_r, msg = FacebookOps.goEditProfile('添加所在地') --"你生活过的地方"
  if flag_r ~= RET_SUCCEEDED then
    return RET_NORMAL_FAILED, "Set place failed. "..msg
  end
  
  AutomatorApi:swipe(317,190,307,470,50)
  mSleep(5000)
  flag_r = AutomatorApi:waitNewWindowByDescEqual("所在地",  20000)
  if flag_r == false then
    return RET_NORMAL_FAILED, "Set place failed. Can't find object by text 所在地."
  end
  
  AutomatorApi:click(627,295)
  AutomatorApi:inputText(city)
  mSleep(3000)
  AutomatorApi:click(355,382)
  mSleep(1000)
  
  if town ~= nil and town ~= '' then
    AutomatorApi:click(627,515)
    AutomatorApi:inputText(town)
    mSleep(3000)
    AutomatorApi:click(345,597)
    mSleep(1000)
  end
  
  AutomatorApi:swipe(317,470,307,190,20)
  mSleep(2000)
  object = uiDevice:findObject(By:desc("保存"))
  if object == nil then
    return RET_NORMAL_FAILED, "Set place failed. Can't find object by text 保存."
  end
  object:click()
  mSleep(5000)
  return RET_SUCCEEDED, "Set place succeeded."
end

--设置Phone
function FacebookOps.setPhone()
  
end


--结束命令
function FacebookOps.doEnd()
  return RET_SUCCEEDED, 'End.'
end

--根据指令执行任务
function FacebookOps.doTask(data)
	local cmd = data[1]
	local ret, msg
  ret = RET_NORMAL_FAILED
	msg = "Uknown cmd."
	
	if cmd == CMD_FB_LOGIN then
		ret, msg = FacebookOps.login(data[2], data[3])
	elseif cmd == CMD_FB_REGISTER then
		ret, msg = FacebookOps.register(nil, nil, data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9])
		if ret == RET_SUCCEEDED then
			FacebookOps.strCurUsr = data[2]
		end
	else
    repeat
--			if FacebookOps.strCurUsr == "" then
--				ret, msg = RET_NORMAL_FAILED, "not login!"
--				break
--			end
			
			if cmd == CMD_FB_ADD_CONTACT_FRIEND then
				ret, msg = FacebookOps.addContactFriend(data[2])
			elseif cmd == CMD_FB_SET_HEAD then
				ret, msg = FacebookOps.setHead(data[2])
			elseif cmd == CMD_FB_SET_COVER then
				ret, msg = FacebookOps.setCover(data[2])
      elseif cmd == CMD_FB_END then
        ret, msg = FacebookOps.doEnd()
			end
		until true
	end
	
	return ret, msg
end
