require 'SharedDic'

CommonOps = {}

--重置IMEI
function CommonOps.resetImei(imei)
  SharedDic.set('imei', imei)
  
	return RET_SUCCEEDED, "Reset imei succeeded"
end

--检查hook生效
function CommonOps.checkHook()
  --检查IMEI
  local local_imei = AutomatorApi:getDeviceId() or '000000000000000'
  if local_imei ~= SharedDic.get('imei') then
    return RET_NORMAL_FAILED, 'Check hook failed. Local imei('..local_imei..') is not equal to offered imei('..SharedDic.get('imei')..').'
  end
  
    --android_id
	local android_id = AutomatorApi:Md5_16(local_imei)
	AutomatorApi:executeShellCommand("settings put secure android_id "..android_id)
  local android_id_new = AutomatorApi:executeShellCommand("settings get secure android_id")
  android_id_new = string.gsub(android_id_new, "\n", "")
  if android_id_new ~= android_id then
    return RET_NORMAL_FAILED, 'Check hook failed. Set android id failed.'
  end
  
  return RET_SUCCEEDED, ""
end


function CommonOps.sleep(ms)
	if ms == nil then
		return false, "Sleep failed. Parameter is null."
	end
	
  local needBroadcast = false
	ms = tonumber(ms)
  if ms > 600000 then
    needBroadcast = true
    AutomatorApi:executeShellCommand("am broadcast -a com.rzx.receiver.WxBroadcastReceiver --es cmd 'startMonkey'")
  end
  
	while ms > 0 do
		AutomatorApi:log("sleep", 'Sleep leave '..tostring(ms)..'ms')
		mSleep(3000)
		ms = ms - 3000
	end
  
  if needBroadcast then
    AutomatorApi:executeShellCommand("am broadcast -a com.rzx.receiver.WxBroadcastReceiver --es cmd 'stopMonkey'")
  end

	return RET_SUCCEEDED, "Ok, I was awake."
end

--通过文件导入通讯录
--contacts_file 需要导入的文件
function CommonOps.addContacts(contacts_file)
  local iRet, sRet = pcall( function() AutomatorApi:addContacts(contacts_file) end)
  if not iRet then
    return RET_NORMAL_FAILED, 'Add contacts failed. '..(sRet or '')
  end
  
  return RET_SUCCEEDED, 'Add contacts succeeded.'
end

--清空通讯录
function CommonOps.clearContacts()
  local iRet, sRet = pcall( function() AutomatorApi:clearContacts() end)
  if not iRet then
    return RET_NORMAL_FAILED, 'Clear contacts failed. '..(sRet or '')
  end
  
  return RET_SUCCEEDED, 'Clear contacts succeeded.'
end

function CommonOps.doTask(data)
	local ret, msg
	local cmd = data[1]
	if cmd == CMD_RESET_IMEI then
		ret, msg = CommonOps.resetImei(data[2])
	elseif cmd == CMD_SLEEP then
		ret, msg = CommonOps.sleep(data[2])
  elseif cmd == CMD_ADD_CONTACTS then
    if data[2] ~= "" then
      local contacts_file = getPath().."/tmp/gh_task/"..SharedDic.get("task_id").."/"..data[2]
      if AutomatorApi:fileExists(contacts_file) then
        ret, msg = CommonOps.addContacts(contacts_file)
      end
    end 
  elseif cmd == CMD_CLEAR_CONTACTS then
    ret, msg = CommonOps.clearContacts()
	end
	
	return ret, msg
end