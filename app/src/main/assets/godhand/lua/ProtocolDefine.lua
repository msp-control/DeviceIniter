--协议固定8个字节
--高四位表示哪个应用相关操作, 低四位表示该应用相关命令, 
--如 "00000001"  
--	  0000 表示微信应用
--    0001 表示登录命令
--"99999999" 为异常错误码

----------------------------------------------------------
--异常错误码
CMD_ERROR = "99999999"

----------------------------------------------------------
--全局相关命令
CMD_LUA_EXIT = "90000001"
CMD_UPLOAD_FILE = "90000002"
CMD_DOWNLOAD_FILE = "90000003"
CMD_RESET_IMEI = "90000004"
CMD_SLEEP = "90000005"
CMD_ADD_CONTACTS = "90000006" --导入通讯录
CMD_CLEAR_CONTACTS = "90000007" --清空通讯录

----------------------------------------------------------
--微信相关命令
CMD_WX_LOGIN_BY_PHONE = "00000000" --手机号登录
CMD_WX_LOGIN = "00000001"	--微信号登录
CMD_WX_LOGOUT = "00000002"	--登出
--CMD_WX_CHANG_USR = "00000003"	--切换用户
CMD_WX_ADD_FRIEND = "00000004"	--添加好友
CMD_WX_CREATE_GROUP = "00000005"	--创建群聊
CMD_WX_SEND_MSG = "00000006"	--发送单聊
CMD_WX_SEND_GROUP_MSG = "00000007"	--发送群聊
CMD_WX_ADD_MP = "00000008"	--添加公众号
CMD_WX_COMMENT_SNS = "00000009"	--评论朋友圈
CMD_WX_UPVOTE_SNS = "00000010"	--点赞朋友圈
CMD_WX_VOTE = "00000011"	--微信投票
CMD_WX_SEND_SNS = "00000012"	--发朋友圈(仅文字)
CMD_WX_UPDATE_NICKNAME = "00000013"	--更改昵称
CMD_WX_ADD_NEAR_FRIEND = "00000014"	--通过附近的人添加好友
CMD_WX_UPDATE_SEX = "00000015"	--更改性别
CMD_WX_RANDOM_SEND_MSG = "00000016"	--随机给好友发消息
CMD_WX_RANDOM_SEND_SNS = "00000017"	--随机发朋友圈
CMD_WX_CLEAR_MSG_RECORD = "00000018" --清除聊天记录
CMD_WX_REGISTER = "00000019"	--注册微信
CMD_WX_SET_ALIAS = "00000020"	--设置微信号
CMD_WX_CLEAR_CACHE = "00000021"	--清除微信缓存
CMD_WX_END = "00000022"	--微信命令结束标志
CMD_WX_SET_AUTO_DOWNLOAD_APK = "00000023"	--设置自动下载微信安装包
CMD_WX_OPEN_URL_BY_WX = "00000024"	--通过微信自带浏览器打开URL
CMD_WX_SET_AREA = "00000025" --设置地区
CMD_WX_SET_HEAD = "00000026" --设置头像
CMD_WX_SET_SNS_BACKGROUND = "00000027" --设置朋友圈背景
CMD_WX_SEND_SNS_WITH_PICTURE = "00000028"	--发朋友圈(带图片)
CMD_WX_ADD_CONTACT_FRIEND = "00000029"  --添加通讯录好友
CMD_WX_SHARE_URL = "00000030" --朋友圈分享链接

----------------------------------------------------------
--facebook相关命令
CMD_FB_REGISTER = "10000000" --注册
CMD_FB_LOGIN = "10000001"	--登录成功
CMD_FB_LOGOUT = "10000002"	--登出
CMD_FB_ADD_CONTACT_FRIEND = "10000003"	--添加通讯录好友
CMD_FB_SET_HEAD = "10000004" --设置头像
CMD_FB_SET_COVER = "10000005" --设置封面
CMD_FB_END = "10000006" --facebook结束任务命令


----------------------------------------------------------
--返回值定义
RET_SUCCEEDED = 					1	--执行成功
RET_NORMAL_FAILED = 				2	--执行失败，但可以继续尝试重试任务
--登录或注册返回定义
RET_LOGIN_USR_OR_PWD_ERR = 			3	--失败，账号密码错误
RET_NEED_SMS_VERIFY_FAILED =		4	--失败，需要短信验证
RET_ACCOUNT_LOCKED =				5	--失败，账号被锁定
RET_FRIEND_VERIFY_FAILED =			6	--失败，好友验证失败
RET_LOGIN_EXPIRED =					7	--失败，登录已过期
RET_LOGIN_ENV_ABNORMAL =			8	--失败，登录环境异常
RET_INVALID_EMAIL =                 9   --失败，无效的邮箱

--错误的任务包
RET_ERROR_TASK =        998 --错误的任务包
--未知错误返回定义
RET_UNKNOWN_FAILED =				999	--执行失败，未知失败
