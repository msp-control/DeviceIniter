require 'FacebookOps'

function testFbSetCover()
  local ret, msg
  local cur_fb_email
  local iRet,sRet = pcall(function () 
    cur_fb_email = AutomatorApi:readFile(getPath().."/tmp/cur_fb_email.txt")
  end)

  if iRet == false then
    AutomatorApi:toast("当前没有产生成功注册账号")
    AutomatorApi:logAppend("FbSetCover", "当前没有产生成功注册账号")
    mSleep(2000)
  else
    cur_fb_email = string.gsub(cur_fb_email, "\n", "")
    local cover_file = getPath().."/tmp/fb_info/"..cur_fb_email.."/cover.jpg"
    
    msg = ""
    local iRet, sRet = pcall(function()
      ret, msg = FacebookOps.setCover(cover_file)
    end)
    if iRet == false then
      msg = sRet
    end
    
    AutomatorApi:logAppend("FbsetCover", cur_fb_email.."\t"..msg)
    AutomatorApi:toast(msg)
    mSleep(200)
  end
end

testFbSetCover()
AutomatorApi:executeShellCommand("busybox ps -ef|busybox grep app_process |busybox grep -v 'grep'|busybox grep instrument|busybox awk '{print $1}'|busybox xargs kill -9")
