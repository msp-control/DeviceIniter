require 'GHLib'
require 'ProtocolDefine'
require 'Configs'
import 'android.support.test.uiautomator.Until'
import 'android.support.test.uiautomator.UiSelector'
import 'android.support.test.uiautomator.By'
import 'com.rzx.godhandmator.ParamsGenerator'


WechatOps = {strCurUsr = ""}
local ALREAD_REGISTERED = false

function WechatOps.loginTest(usr, pwd)
  local ret, msg
  local original_cache_path = '/data/data/com.tencent.mm'
  local usr_cache_path = original_cache_path..'_'..usr
  local stat_err_tmpf = getPath()..'/tmp/stat_err.txt'
  local airplaneSwitch = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'airplane_switch', 'true')
  AutomatorApi:executeShellCommand("am force-stop com.tencent.mm")
  AutomatorApi:executeShellCommand('rm -fr /sdcard/tencent/MicroMsg/')
  if airplaneSwitch == 'true' then
    AutomatorApi:toast("开启飞行模式")
    openAirplaneMode()
    AutomatorApi:toast("关闭飞行模式")
    closeAirplaneMode()
  end
  
  AutomatorApi:executeShellCommand('busybox stat '..usr_cache_path..' 2>'..stat_err_tmpf)
  local stat_err_txt = AutomatorApi:readFile(stat_err_tmpf)
  if stat_err_txt and stat_err_txt ~= '' then
    if string.find(stat_err_txt, 'No such file or directory') then
      --新建一个目录，并权限设为任何用户可读写可执行
      AutomatorApi:executeShellCommand('mkdir '..usr_cache_path)
--      local uid = AutomatorApi:executeShellCommand("busybox ls -lAL|grep 'com.tencent.mm'|busybox head|busybox awk '{print $3}'")
--      uid = string.gsub(uid, "\n", "")
      AutomatorApi:executeShellCommand('chown 777 '..usr_cache_path)
      
      --删除软链接
      AutomatorApi:executeShellCommand('rm -fr '..original_cache_path)
      --生成账号对应设备属性
      ParamsGenerator:generateParams(context, SharedDic.get('imei'))
      --保存设备信息
      AutomatorApi:executeShellCommand('cp /sdcard/GodHand/res/param.properties '..usr_cache_path)
      
      --检查设备属性更改是否生效
      ret, msg = CommonOps.checkHook()
      if ret ~= RET_SUCCEEDED then
        return RET_NORMAL_FAILED, usr..' Login failed. '..msg
      end
    
      --建立软连接到指定登录账号
      AutomatorApi:executeShellCommand('busybox ln -sf '..usr_cache_path..' '..original_cache_path)
      --修改软链接权限,手机重启会对所有app的/data/data/目录下的权限进行检测
      local uid = AutomatorApi:executeShellCommand("cat /data/system/packages.list|grep 'com.tencent.mm '|busybox awk '{print $2}'")
      uid = string.gsub(uid, "\n", "") or '0'
      AutomatorApi:executeShellCommand('busybox chown -H '..uid..':'..uid..' '..original_cache_path)
      
      --登录
      ret, msg = WechatOps.loginByInput(usr, pwd)
    else
      return RET_NORMAL_FAILED, usr..' Login failed. '..stat_err_txt
    end
  else
    --删除软连接
    AutomatorApi:executeShellCommand('rm -fr '..original_cache_path)
    --设置账号对应设备属性
    local proFile = AutomatorApi:executeShellCommand("ls "..usr_cache_path.."/*.properties")
    if proFile ~= nil and proFile ~= '' then
      AutomatorApi:executeShellCommand('cp -a '..usr_cache_path..'/*.properties '..getPath()..'/res/')
    else
      ParamsGenerator:generateParams(context, SharedDic.get('imei'))
      --保存设备信息
      AutomatorApi:executeShellCommand('cp /sdcard/GodHand/res/param.properties '..usr_cache_path)
    end
    --检查设备属性更改是否生效
    ret, msg = CommonOps.checkHook()
    if ret ~= RET_SUCCEEDED then
      return RET_NORMAL_FAILED, usr..' Login failed. '..msg
    end
  
    --建立软连接到指定登录账号
    AutomatorApi:executeShellCommand('busybox ln -sf '..usr_cache_path..' '..original_cache_path)
    --修改软链接权限,手机重启会对所有app的/data/data/目录下的权限进行检测
    local uid = AutomatorApi:executeShellCommand("cat /data/system/packages.list|grep 'com.tencent.mm '|busybox awk '{print $2}'")
    uid = string.gsub(uid, "\n", "") or '0'
    AutomatorApi:executeShellCommand('busybox chown -H '..uid..':'..uid..' '..original_cache_path)
    --登录
    ret, msg = WechatOps.loginByCache(nil, usr, pwd)
  end
  
  if ret == RET_SUCCEEDED then
		WechatOps.strCurUsr = usr
    if AutomatorApi:waitNewWindowByTextEqual('腾讯新闻', 5000) then
      AutomatorApi:clickByTextEqual('腾讯新闻', 0)
      mSleep(5000)
    end
	end  
	
	return ret, msg
end
                              

--登录
function WechatOps.login(usr, pwd)
  if SharedDic.get('test_wx_switch') == 'true' then
    return WechatOps.loginTest(usr, pwd)
  end
  
	local ret, msg
	ret = RET_SUCCEEDED
	msg = usr..' Login succeeded.'
  
  AutomatorApi:executeShellCommand("am force-stop com.tencent.mm")
	mSleep(1000)
	AutomatorApi:executeShellCommand("rm -fr /sdcard/tencent/MicroMsg/*")
	AutomatorApi:executeShellCommand("rm -fr /data/data/com.tencent.mm/*")
	mSleep(1000)
  
  local airplaneSwitch = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'airplane_switch', 'true')
  if airplaneSwitch == 'true' then
    AutomatorApi:toast("开启飞行模式")
    openAirplaneMode()
    AutomatorApi:toast("关闭飞行模式")
    closeAirplaneMode()
  end
  
	SharedDic.set('usrId', usr)
	local cache_file = getPath().."/res/cache/com.tencent.mm/"..usr
	local tar_file = getPath().."/tmp/gh_task/"..SharedDic.get("task_id").."/"..usr..".tar.gz"
	
	if AutomatorApi:fileExists(tar_file) then
		AutomatorApi:executeShellCommand("rm -fr "..getPath().."/res/cache/com.tencent.mm/*")
		AutomatorApi:executeShellCommand("mkdir -p "..getPath().."/res/cache/com.tencent.mm/")
		AutomatorApi:executeShellCommand("busybox tar -zxf "..tar_file.." -C "..getPath().."/res/cache/com.tencent.mm/")
	end
	
  --缓存是否存在
	if AutomatorApi:fileExists(cache_file) then
    --hook参数信息如果存在，放到res目录下, 不存在生成
    local proFile = AutomatorApi:executeShellCommand("ls "..cache_file.."/*.properties")
    if proFile ~= nil and proFile ~= '' then
      AutomatorApi:executeShellCommand('cp -a '..cache_file..'/*.properties '..getPath()..'/res/')
    else
      ParamsGenerator:generateParams(context, SharedDic.get('imei'))
    end
    ret, msg = CommonOps.checkHook()
    if ret ~= RET_SUCCEEDED then
      return RET_NORMAL_FAILED, usr..' Login failed. '..msg
    end
      
    --缓存登录
		ret,msg = WechatOps.loginByCache(cache_file, usr, pwd)
		
		--如果第一次登录发生正常类型的错误，则重试一次
--		if ret == RET_NORMAL_FAILED then
--			ret,msg = WechatOps.loginByCache(cache_file, usr, pwd)
--		end
	else
    --生成hook信息文件
    ParamsGenerator:generateParams(context, SharedDic.get('imei'))
    ret, msg = CommonOps.checkHook()
    if ret ~= RET_SUCCEEDED then
      return RET_NORMAL_FAILED, usr..' Login failed. '..msg
    end
    
		ret,msg = WechatOps.loginByInput(usr, pwd)
		
		--如果第一次登录发生正常类型的错误，则重试一次
--		if ret == RET_NORMAL_FAILED then
--			ret,msg = WechatOps.loginByInput(usr, pwd)
--		end
	end
	
	if ret == RET_SUCCEEDED then
		WechatOps.strCurUsr = usr
		AutomatorApi:executeShellCommand("mkdir -p /sdcard/GodHand/tmp/tencent/MicroMsg")
		AutomatorApi:executeShellCommand("cp -a /data/data/com.tencent.mm/MicroMsg/* /sdcard/GodHand/tmp/tencent/MicroMsg")
		local uin = AutomatorApi:getWechatUin("/sdcard/GodHand/tmp/tencent")
		SharedDic.set('uin', uin)
    
    if AutomatorApi:waitNewWindowByTextEqual('腾讯新闻', 5000) then
      AutomatorApi:clickByTextEqual('腾讯新闻', 0)
      mSleep(5000)
    end
	end
	
	return ret, msg
end

--登录一些弹框判断
local function loginPromptDialogJudge(usr, pwd)
  local object
  
  --更新提示
  object = uiDevice:findObject(By:text('不喜欢'))
  if object ~= nil then
    object:click()
    if AutomatorApi:waitNewWindowByTextEqual('不再显示', 1000) then
      AutomatorApi:clickByTextEqual('不再显示', 0)
    end
  end
  
  --更新提示
  object = uiDevice:findObject(By:text('立刻安装'))
  if object ~= nil then
    object = uiDevice:findObject(By:text('取消'))
    if object ~= nil then
      object:click()
      mSleep(1000)
      object = uiDevice:findObject(By:text('是'))
      if object ~= nil then
        object:click()
      end
    end
  end
  
  --申请解封提示
  object = uiDevice:findObject(By:textContains("申请解封"))
  if object ~= nil then
    return RET_ACCOUNT_LOCKED, usr..' Login failed. Account locked, need unlock.'
  end
  
  --提示框
  object = uiDevice:findObject(By:text('提示'))
  if object ~= nil then
    object = uiDevice:findObject(By:textContains('重新登录'))
    if object ~= nil then
      return RET_NORMAL_FAILED, usr..' Login failed. Need login again by input.Please enable login_by_input and then login by input again.'
    end
    
    object = uiDevice:findObject(By:textContains('已经泄露'))
    if object ~= nil then
      return RET_NORMAL_FAILED, usr..' Login failed. Your account has been logined in another device. Please enable login_by_input and then login by input again.'
    end
  end
  
  --登录环境异常
  object = uiDevice:findObject(By:textContains('登录环境异常'))
  if object ~= nil then
    return RET_LOGIN_ENV_ABNORMAL, usr..' Login failed. Login environment abnormal.'
  end
  
  --登录错误
  object = uiDevice:findObject(By:textContains('登录错误'))
  if object ~= nil then
    return RET_NORMAL_FAILED, usr..' Login failed. Login error prompt.Please enable login_by_input and then login by input again.'
  end
  
  return RET_SUCCEEDED
end

--缓存方式登录
function WechatOps.loginByCache(cache_file, usr, pwd)
	AutomatorApi:toast("loginByCache\n"..usr)
  mSleep(2000)

	AutomatorApi:executeShellCommand("am start -n com.tencent.mm/.ui.LauncherUI")
	
  local object = nil
	local timeout = 30000
	while timeout > 0 do
		local top_activity = getTopActivity()
		--欢迎滑动界面（滑动几下，点击进入微信）
		if top_activity == "com.tencent.mm/.plugin.whatsnew.ui.WhatsNewUI" then
			AutomatorApi:swipe(631,638, 37, 586, 30)
			mSleep(1500)
			AutomatorApi:swipe(631,638, 37, 586, 30)
			mSleep(1500)
			AutomatorApi:swipe(631,638, 37, 586, 30)
			mSleep(1500)
			AutomatorApi:swipe(631,638, 37, 586, 30)
			mSleep(1500)
			AutomatorApi:click(335, 991)
			mSleep(5000)
		end
		
    --微信开始页面（无账号信息）
    object = uiDevice:findObject(By:text('登录'))
		if object ~= nil then
      object = uiDevice:findObject(By:text('注册'))
      if object ~= nil then
        if SharedDic.get('enable_login_by_input') == 'false' then
          return RET_NORMAL_FAILED, usr..' Login failed. Cache file is not avaliable.'
        end
        
        return WechatOps.loginByInput(usr, pwd)
      end
		end
		
    --微信开始页面（有账号信息）
    object = uiDevice:findObject(By:text('登录遇到问题？'))
    if object ~= nil then
      if SharedDic.get('enable_login_by_input') == 'false' then
        return RET_NORMAL_FAILED, usr..' Login failed. Cache file is not avaliable.'
      end
      return WechatOps.loginByInput(usr, pwd)
    end
    
    --提示框内容判断
    local r, s = loginPromptDialogJudge(usr, pwd)
    if r ~= RET_SUCCEEDED then
      if r == RET_NORMAL_FAILED then
        if SharedDic.get('enable_login_by_input') == 'false' then
          return r,s
        end
        return WechatOps.loginByInput(usr, pwd)
      else
        return r,s
      end
    end
		
    --进入登录主页面
    object = uiDevice:findObject(By:text('发现')) and
      uiDevice:findObject(By:text('通讯录')) and
      uiDevice:findObject(By:text('微信'))
		if object ~= nil then
      local timeout = 20000
      while timeout > 0 do
        --提示框内容判断
        local r, s = loginPromptDialogJudge(usr, pwd)
        if r ~= RET_SUCCEEDED then
          if r == RET_NORMAL_FAILED then
            if SharedDic.get('enable_login_by_input') == 'false' then
              return r,s
            end
            return WechatOps.loginByInput(usr, pwd)
          else
            return r,s
          end
        end
        mSleep(1000)
        timeout = timeout - 1000
      end
      
      object = uiDevice:findObject(By:text('发现')) and
        uiDevice:findObject(By:text('通讯录')) and
        uiDevice:findObject(By:text('微信'))
      if object ~= nil then
        return RET_SUCCEEDED, usr..' Login succeeded.'
      else
        return RET_UNKNOWN_FAILED, usr..' Login failed. unknown error.'
      end
		end
		timeout = timeout - 1000
		mSleep(1000)
	end
	
	return RET_UNKNOWN_FAILED, usr..' Login failed. unknown error.'
end

--输入方式登录
function WechatOps.loginByInput(usr, pwd)
  if SharedDic.get('enable_login_by_input') == 'false' then
    return RET_NORMAL_FAILED, usr..' Login failed. login_by_input is disabled.'
  end
	AutomatorApi:toast("loginByInput\n"..usr)
  mSleep(2000)
  
	AutomatorApi:executeShellCommand("am start --activity-no-history com.tencent.mm/.ui.account.LoginUI")
	ret = AutomatorApi:waitNewWindowByTextContain("登录微信", 30000)
	if(ret == false) then return RET_NORMAL_FAILED, usr.." Login failed. Can't find login UI." end
	AutomatorApi:setTextByClass("android.widget.EditText", usr, 0)
	AutomatorApi:setTextByClass("android.widget.EditText", pwd, 1)
	AutomatorApi:clickByTextEqual("登录", 0)
	
	local timeout = 20000
	while timeout > 0 do
		if AutomatorApi:waitNewWindowByTextContain("帐号或密码错误", 1) then return RET_LOGIN_USR_OR_PWD_ERR, usr.." Login failed. User or password wrong." end
		if AutomatorApi:waitNewWindowByTextContain("通过短信验证身份", 1) then return RET_NEED_SMS_VERIFY_FAILED, usr.." Login failed. Need sms verify." end
		if AutomatorApi:waitNewWindowByTextContain("申请解封", 1) then return RET_ACCOUNT_LOCKED, usr.." Account locked. Need unlock." end
		if AutomatorApi:waitNewWindowByTextContain('登录环境异常', 1) then return RET_LOGIN_ENV_ABNORMAL, usr.." Login failed. Login environment abnormal." end
		
		--好友验证
		if AutomatorApi:waitNewWindowByTextContain("你登录的微信需要进行好友验证", 1) then
			AutomatorApi:clickByTextEqual("确定", 0)
			x,y = findMultiColorInRegionFuzzyInTime( 0x04be02, "-303|-7|0x04be02,317|2|0x04be02,34|-237|0x7cb550,22|66|0xefeff4", 90, 0, 0, 719, 1279, 10000)
			if x ==-1 or y == -1 then
				return RET_FRIEND_VERIFY_FAILED, usr.." Login failed. Friends verify failed."
			end
			AutomatorApi:click(x, y)
			return WechatOps.loginByInput(usr, pwd)
		end
		
		--提示'看看手机通讯录里谁在使用微信'
		if AutomatorApi:waitNewWindowByTextContain('看看手机通讯录里谁在使用微信', 1) then
			AutomatorApi:clickByTextEqual("否", 0)
		end
		
		if AutomatorApi:waitNewWindowByTextEqual('发现', 1) and 
		   AutomatorApi:waitNewWindowByTextEqual('通讯录', 1) and 
		   AutomatorApi:waitNewWindowByTextEqual('微信', 1) then
			return RET_SUCCEEDED, usr..' Login succeeded.'
		end
		timeout = timeout - 2000
	end
	return RET_UNKNOWN_FAILED, usr..' Login failed. Unknown error.'
end

function WechatOps._alreadyRegisteredConfirmClicked(phone_num, password)
	local ret, msg
  local object
	
	local time_out2 = 25000
	while time_out2 > 0 do
    object = uiDevice:findObject(By:textContains('申请解封'))
		if object ~= nil then
			return RET_ACCOUNT_LOCKED, "Register failed. Account locked."
		end
    
		if uiDevice:findObject(By:textContains('长期未登录')) then
			return RET_ACCOUNT_LOCKED, "Register failed. Account locked. Not login for a long time."
		end
		
		if uiDevice:findObject(By:textContains('已经被删除')) then
			AutomatorApi:clickByTextEqual("确定", 0)
			if AutomatorApi:waitNewWindowByTextContain('该手机号已经绑定如上微信', 2000) then
				AutomatorApi:clickByTextEqual("不是我的，继续注册", 0)
			end
		end
	
		if uiDevice:findObject(By:textContains("你登录的微信需要进行好友验证")) then
			AutomatorApi:clickByTextEqual("确定", 0)
			
			local time_out3 = 25000
			while time_out3 > 0 do
				x,y = findMultiColorInRegionFuzzy( 0x04be02, "561|3|0x04be02,270|-228|0x7bb44f,147|-273|0xefeff4,251|-146|0x000000", 90, 0, 0, 719, 1279)
				if x ~= -1 and y ~= -1 then
					mSleep(1000)
					AutomatorApi:click(48, 99)
					break
				end
				
				if uiDevice:findObject(By:textContains("进入微信")) then
					AutomatorApi:clickByTextEqual("进入微信", 0)
					break
				end
				time_out3 = time_out3 - 1500
			end
			
			if AutomatorApi:waitNewWindowByTextContain('该手机号已经绑定如上微信', 2000) then
				AutomatorApi:clickByTextEqual("不是我的，继续注册", 0)
			end
		end

    ----登录成功主界面判断
    object = uiDevice:findObject(By:text('发现'))
    if object ~= nil then
      object = uiDevice:findObject(By:text('通讯录'))
      if object ~= nil then
        WechatOps.strCurUsr = phone_num
        mSleep(10000)
        return RET_SUCCEEDED, "Register reset. This phone has been registerd."
      end
    end
		
		--提示'看看手机通讯录里谁在使用微信'
		if uiDevice:findObject(By:textContains('看看手机通讯录里谁在使用微信')) then
			AutomatorApi:clickByTextEqual("否", 0)
			
			local time_out3 = 25000
      local object
			while time_out3 > 0 do
        object = uiDevice:findObject(By:text('设置密码'))
				if object ~= nil then
					mSleep(1000)
					AutomatorApi:setTextByClass("android.widget.EditText", password, 0)
					AutomatorApi:setTextByClass("android.widget.EditText", password, 1)
					AutomatorApi:clickByTextEqual("完成", 0)
					
					if AutomatorApi:waitNewWindowByTextContain('看看手机通讯录里谁在使用微信', 20000) then
						AutomatorApi:clickByTextEqual("否", 0)
					end
				end
				
        object = uiDevice:findObject(By:text('发现'))
				if object ~= nil then
          object = uiDevice:findObject(By:text('通讯录'))
          if object ~= nil then
            ALREAD_REGISTERED = true
            WechatOps.strCurUsr = phone_num
            mSleep(10000)
            return RET_SUCCEEDED, "Register reset. This phone has been registerd."
          end
				end
				
				time_out3 = time_out3 - 1500
			end
			
		end
		time_out2 = time_out2 - 1000
    mSleep(1000)
	end
	
	return RET_UNKNOWN_FAILED, "Register failed. Unknown error."
end


--注册
function WechatOps.register(nick, country_code, phone_num, password)
	local ret, msg
  
  --关闭应用
  AutomatorApi:executeShellCommand("am force-stop com.tencent.mm")
  AutomatorApi:executeShellCommand('rm -fr /sdcard/tencent/MicroMsg/')
  
  --新建一个目录，并权限设为任何用户可读写可执行
  local original_cache_path = '/data/data/com.tencent.mm'
  local usr_cache_path = '/data/data/register_com.tencent.mm'
  AutomatorApi:executeShellCommand('rm -fr '..usr_cache_path)
  AutomatorApi:executeShellCommand('mkdir '..usr_cache_path)
  AutomatorApi:executeShellCommand('chmod 777 '..usr_cache_path)
  AutomatorApi:executeShellCommand("echo "..password.." >"..usr_cache_path.."/password")
  AutomatorApi:executeShellCommand('rm -fr '..original_cache_path)
  
  --删除软链接
  AutomatorApi:executeShellCommand('rm -f '..original_cache_path)
  --生成账号对应设备属性
  ParamsGenerator:generateParams(context, SharedDic.get('imei'))
  --保存设备信息
  AutomatorApi:executeShellCommand('cp /sdcard/GodHand/res/param.properties '..usr_cache_path)
  --检查设备属性更改是否生效
  ret, msg = CommonOps.checkHook()
  if ret ~= RET_SUCCEEDED then
    return RET_NORMAL_FAILED, usr..' Login failed. '..msg
  end

  --建立软连接到指定登录账号
  AutomatorApi:executeShellCommand('busybox ln -sf '..usr_cache_path..' '..original_cache_path)
  --修改软链接权限,手机重启会对所有app的/data/data/目录下的权限进行检测
  local uid = AutomatorApi:executeShellCommand("cat /data/system/packages.list|grep 'com.tencent.mm '|busybox awk '{print $2}'")
  uid = string.gsub(uid, "\n", "") or '0'
  AutomatorApi:executeShellCommand('busybox chown -H '..uid..':'..uid..' '..original_cache_path)
  
--	AutomatorApi:executeShellCommand("am force-stop com.tencent.mm")
--	mSleep(2000)
--	AutomatorApi:executeShellCommand("rm -fr /data/data/com.tencent.mm/*")
--  AutomatorApi:executeShellCommand("rm -fr /sdcard/tencent/MicroMsg/*")
  
  local airplaneSwitch = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'airplane_switch', 'true')
  if airplaneSwitch == 'true' then
    AutomatorApi:toast("开启飞行模式")
    openAirplaneMode()
    AutomatorApi:toast("关闭飞行模式")
    closeAirplaneMode()
  end
  
  local ok_time_out = 60000
  local network_avaliable
  while ok_time_out > 0 do
    network_avaliable = check_network_avaliable()
    if network_avaliable then break end
    ok_time_out = ok_time_out - 1000
    mSleep(1000)
  end
  
  if ok_time_out <= 0 then
    return RET_NORMAL_FAILED, "Register failed. Network is not avaliable."
  end
  
  local ip = AutomatorApi:executeShellCommand("curl -s whatismyip.akamai.com")
  AutomatorApi:logAppend("Register_total", phone_num..'\t'..ip)
	
--	AutomatorApi:executeShellCommand("am start --activity-no-history com.tencent.mm/.ui.account.RegByMobileRegAIOUI")
  AutomatorApi:executeShellCommand("am start -W -n com.tencent.mm/.ui.LauncherUI")
  ret = AutomatorApi:waitNewWindowByTextEqual('注册', 25000)
  if ret == false then
    ret = RET_NORMAL_FAILED
    msg = "Register failed. Can't open com.tencent.mm/.ui.LauncherUI."
    return ret, msg
  end
  AutomatorApi:clickByTextEqual('注册', 0)
  
	repeat
		ret = AutomatorApi:waitNewWindowByTextEqual('填写手机号', 25000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Register failed. Can't open register window."
			return ret, msg
		end
		AutomatorApi:clickByClass("android.widget.EditText", 0)
		AutomatorApi:inputText(nick)
		AutomatorApi:setTextByClass("android.widget.EditText", country_code, 1)
		AutomatorApi:setTextByClass("android.widget.EditText", phone_num, 2)
		AutomatorApi:setTextByClass("android.widget.EditText", password, 3)
		AutomatorApi:clickByTextEqual("注册", 0)
		
		--确认手机号码提示框
		ret = AutomatorApi:waitNewWindowByTextEqual('确认手机号码', 35000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Register failed. Can't go to confirm prompt UI."
			return ret, msg
		end
		AutomatorApi:clickByTextEqual("确定", 0)
		
		if AutomatorApi:waitNewWindowByTextContain('操作太频繁', 10000) then
			ret = RET_NORMAL_FAILED
			msg = "Register failed. Too frequent operation."
			return ret, msg
		end
		
		--填写验证码界面
		ret = AutomatorApi:waitNewWindowByTextEqual('填写验证码', 30000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Register failed. Can't go to sns verify input window."
			return ret, msg
		end
		
		
		--获取验证码并输入验证码
		local sms
		ret, sms = TaskInterface.getSms(AutomatorApi:readFile(getPath().."/uuid.txt"), SharedDic.get("task_id"), phone_num, 120000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Register failed. "..sms
			return ret, msg
		end
		
		AutomatorApi:click(286, 407)
		AutomatorApi:inputText(sms)
		mSleep(2000)
		AutomatorApi:clickByTextEqual("下一步", 0)
		
		local time_out = 30000
		while time_out > 0 do
			if AutomatorApi:waitNewWindowByTextContain('验证码不正确', 1) then
				return RET_NORMAL_FAILED, "Register failed. Verify code wrong."
			end
		
			--进入查找朋友界面
			if AutomatorApi:waitNewWindowByTextEqual('查找你的微信朋友', 1) then
				AutomatorApi:clickByTextEqual("好", 0)
				AutomatorApi:waitNewWindowByTextEqual('发现', 20000)
        time_out = 1
				break
			end
			
			if AutomatorApi:waitNewWindowByTextContain('该手机号码已经注册', 1) then
				AutomatorApi:clickByTextEqual("确定", 0)
				return WechatOps._alreadyRegisteredConfirmClicked(phone_num, password)
			end
			
			if AutomatorApi:waitNewWindowByTextContain('该手机号已经绑定如上微信', 1) then
				AutomatorApi:clickByTextEqual("不是我的，继续注册", 0)
				return WechatOps._alreadyRegisteredConfirmClicked(phone_num, password)
			end
			
			time_out = time_out - 2000
		end
		
		if time_out <= 0 then
			ret = RET_UNKNOWN_FAILED
			msg = "Register failed. Unknown error."
			return ret, msg
		end
		
		WechatOps.strCurUsr = phone_num
	until(true)
  
  AutomatorApi:logAppend("Register_suc", phone_num..'\t'..ip)
	return RET_SUCCEEDED, 'Register succeeded.'
end

--添加公众号
function WechatOps.addMp(alias)
	local ret
	local object
	
	AutomatorApi:executeShellCommand("am start --activity-no-history -n com.tencent.mm/.plugin.search.ui.FTSMainUI")
	ret = AutomatorApi:waitNewWindowByTextEqual('搜索', 5000)
	if ret == false then
		return RET_NORMAL_FAILED, "Add mp failed. Can't go to .plugin.search.ui.FTSMainUI"
	end
	AutomatorApi:click(557, 335)
	ret = AutomatorApi:waitNewWindowByTextEqual('搜索公众号', 5000)
	if ret == false then
		return RET_NORMAL_FAILED, "Add mp failed. Can't go to .plugin.search.ui.FTSMainUI"
	end
	
	AutomatorApi:click(250, 100)
	AutomatorApi:inputText(alias)
	mSleep(500)
	AutomatorApi:click(250, 100)
	AutomatorApi:inputText('#Enter')
	mSleep(500)
	
	ret = AutomatorApi:waitNewWindowByDescEqual('公众号 Heading', 20000)
	if ret == false then
		return RET_NORMAL_FAILED, "Add mp failed. Can't find the mp."
	end
	--选择第一个搜索到的公众号
	AutomatorApi:click(264, 320)
	
	ret = AutomatorApi:waitNewWindowByTextEqual('功能介绍', 8000)
	if ret == false then
		return RET_NORMAL_FAILED, "Add mp failed. Can't find the mp."
	end
	
	AutomatorApi:swipe(719,1096,719,150, 10)
	mSleep(1000)
	
	object = uiDevice:findObject(By:text('进入公众号'))
	if object ~= nil then
		return RET_SUCCEEDED, 'Add mp succeeded. Already add the mp.'
	end
	
	object = uiDevice:findObject(By:text('关注'))
	if object == nil then
		return RET_NORMAL_FAILED, "Add mp succeeded. Can't add button."
	end
	AutomatorApi:clickByTextEqual('关注', 0)
	ret = waitNewWindow('com.tencent.mm/.plugin.profile.ui.ContactInfoUI', 15000)
	if ret == false then
		return RET_NORMAL_FAILED, "Add mp failed. Timeout."
	end
	
	return RET_SUCCEEDED, "Add mp succeeded."
end

--根据手机号、QQ号、微信号添加好友
function WechatOps.addFriend(usr, hi_words)
	local ret,msg
	
	if usr == nil or usr == "" then
		return false, 'Add friend failed. Account is null.'
	end
	
	AutomatorApi:executeShellCommand("am start --activity-no-history -n com.tencent.mm/.plugin.search.ui.FTSAddFriendUI")
	repeat 
		mSleep(1000)
		ret = AutomatorApi:waitNewWindowByTextEqual('搜索', 5000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Add friend "..usr.." failed. Can't go to .plugin.search.ui.FTSAddFriendUI"
			return ret, msg
		end
		
		AutomatorApi:setTextByClass("android.widget.EditText", usr, 0)
		AutomatorApi:inputText("#Enter")
		
		local time_out = 30000
		while time_out > 0 do
			if AutomatorApi:waitNewWindowByTextEqual('该用户不存在', 1) then
				return RET_NORMAL_FAILED, "Add friend "..usr.." failed. This user doesn't exist."
			end
			
			if waitNewWindow('com.tencent.mm/.plugin.search.ui.FTSAddFriendUI', 1) then
				return ret, msg
			end
			time_out = time_out - 500
		end
	
		ret = AutomatorApi:waitNewWindowByTextEqual('添加到通讯录', 5000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Add friend "..usr.." failed. Can't find add button."
			return ret, msg
		end
		AutomatorApi:clickByTextEqual('添加到通讯录', 0)
		ret = waitNewWindow('com.tencent.mm/.plugin.profile.ui.ContactInfoUI', 25000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Add friend "..usr.." failed. Network is not avaliable."
			return ret, msg
		end
		
		ret = AutomatorApi:waitNewWindowByTextContain('你需要发送验证申请', 5000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Add friend "..usr.." failed. Can't go to say hi UI."
			return ret, msg
		end
		
		if hi_words ~= nil and hi_words ~= "" then
			AutomatorApi:click(640, 294)
			AutomatorApi:inputText(hi_words)
			mSleep(500)
		end		
		AutomatorApi:clickByTextEqual('发送', 0)
		
		ret = waitNewWindow('com.tencent.mm/.plugin.profile.ui.SayHiWithSnsPermissionUI', 25000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Add friend "..usr.." failed. Network is not avaliable."
			return ret, msg
		end
	until(true)
	
	return RET_SUCCEEDED, "Add "..usr.." succeeded."
end

function WechatOps.getRandText(filename)
	local s = io.open(filename)
	
	local table_lines = {}
	local line
	for line in s:lines() do 
		table.insert(table_lines, line)
	end
	
	return table_lines[math.random(#table_lines)]
end

--发送单聊
function WechatOps.sendMsg(who, message)
	local ret
	local object

	AutomatorApi:executeShellCommand("am start --activity-no-history -n com.tencent.mm/.plugin.search.ui.FTSMainUI")
	ret = AutomatorApi:waitNewWindowByTextEqual('搜索', 5000)
	if ret == false then
		return RET_NORMAL_FAILED, "Send message failed. Can't go to .plugin.search.ui.FTSMainUI"
	end
	
	AutomatorApi:inputText(who)
	mSleep(1000)
	
	object = uiDevice:findObject(By:text('联系人'))
	if object == nil then
		object = uiDevice:findObject(By:text('最常使用'))
		if object == nil then
			return RET_NORMAL_FAILED, "Send message failed. Can't find the person."
		end
	end
	
	--选择第一个联系人
	AutomatorApi:click(269, 310)
	-- AutomatorApi:executeShellCommand("am start --activity-no-history -n com.tencent.mm/.ui.chatting.ChattingUI --es Chat_User '"..who.."'")
	
	ret = AutomatorApi:waitNewWindowByDescEqual('表情', 10000)
	if ret == false then 
		return RET_NORMAL_FAILED, "Send message failed. Can't open chat dialog."
	end
	
	AutomatorApi:clickByClass("android.widget.EditText", 0)
	AutomatorApi:setTextByClass("android.widget.EditText", "", 0)
	
	if SharedDic.get('test_wx_switch') == 'true' then
		-- message = WechatOps.getRandText(getPath().."/res/sns_msg.txt")
		message = AutomatorApi:executeShellCommand("busybox tail -10 /sdcard/GodHand/log/task_detail.log")
		local date_ = AutomatorApi:executeShellCommand("date")
		AutomatorApi:inputText(message.."\n"..date_)
	else
		AutomatorApi:inputText(message)
	end
	AutomatorApi:clickByTextEqual("发送", 0)
	mSleep(2000)
	
	return RET_SUCCEEDED, 'Send message succeeded.'
end

--发带图片朋友圈
function WechatOps.sendSnsWithPicture(content, picPath)
	local ret,msg

	local file_exists = AutomatorApi:fileExists(picPath)
	if file_exists == false then
		return WechatOps.sendSnsJustText(content)
	end
	
	AutomatorApi:executeShellCommand("am start -n com.tencent.mm/.plugin.sns.ui.SnsTimeLineUI")
	AutomatorApi:executeShellCommand("am start --activity-no-history -n com.tencent.mm/.plugin.sns.ui.SnsUploadUI"
		.." --es sns_kemdia_path '"..picPath.."'"
		.." --ei Ksnsupload_type 0")
	
	repeat
		ret = AutomatorApi:waitNewWindowByTextEqual("谁可以看", 3000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Send sns with picture failed. Can't open the plugin.sns.ui.SnsUploadUI."
			return ret, msg
		end
		
		AutomatorApi:setTextByClass("android.widget.EditText", "", 0)
		AutomatorApi:inputText(content)
		mSleep(1000)
		AutomatorApi:clickByTextEqual("发送", 0)
		
		ret = waitNewWindow("com.tencent.mm/.plugin.sns.ui.SnsUploadUI", 25000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Send sns with picture failed. Network is not avaliable."
			return ret, msg
		end
	until(true)
	return RET_SUCCEEDED, "Send sns with picture succeeded."
end

--仅发文字朋友圈
function WechatOps.sendSnsJustText(content)
	local ret,msg
	
	AutomatorApi:executeShellCommand("am start -n com.tencent.mm/.plugin.sns.ui.SnsTimeLineUI")
	
	-- AutomatorApi:executeShellCommand("am start --activity-no-history -n com.tencent.mm/.plugin.sns.ui.SnsUploadUI"
		-- .." --ei Ksnsupload_type 9")
	
	repeat
		ret = AutomatorApi:waitNewWindowByTextEqual("朋友圈", 3000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Send sns with picture failed. Can't go to .plugin.sns.ui.SnsTimeLineUI."
			return ret, msg
		end
		
		AutomatorApi:longClick(662, 104, 1000)
	
		ret = waitNewWindow('com.tencent.mm/.plugin.sns.ui.SnsTimeLineUI', 5000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Send sns with picture failed. Can't go to next UI."
			return ret, msg
		end
	
		if AutomatorApi:waitNewWindowByTextEqual('我知道了', 1) then
			AutomatorApi:clickByTextEqual('我知道了', 0)
		end
		
		ret = AutomatorApi:waitNewWindowByTextEqual("谁可以看", 3000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Send sns with picture failed. Can't open the plugin.sns.ui.SnsUploadUI."
			return ret, msg
		end
		
		AutomatorApi:setTextByClass("android.widget.EditText", "", 0)
		AutomatorApi:inputText(content)
		mSleep(1000)
		AutomatorApi:clickByTextEqual("发送", 0)
		
		waitNewWindow("com.tencent.mm/.plugin.sns.ui.SnsUploadUI", 15000)
	until(true)
	
	return RET_SUCCEEDED, "Send sns with picture succeeded."
end

--添加附近的人
function WechatOps.addNearFriend()
	local ret,msg

	AutomatorApi:executeShellCommand("am start --activity-no-history com.tencent.mm/.plugin.nearby.ui.NearbyFriendsUI")
	
	repeat
		ret = AutomatorApi:waitNewWindowByTextEqual("附近的人", 15000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Add near friend failed. Can't open the plugin.nearby.ui.NearbyFriendsUI."
			return ret, msg
		end

		AutomatorApi:swipe(719,1096,719,150,math.random(5,100))
		mSleep(2000)
		AutomatorApi:click(math.random(9,700), math.random(253,1274))
		ret = AutomatorApi:waitNewWindowByTextEqual("打招呼", 2000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Add near friend failed. Can't open contact info UI."
			return ret, msg
		end
		AutomatorApi:clickByTextEqual("打招呼", 0)
		AutomatorApi:waitNewWindowByTextEqual("向TA说句话打个招呼", 2000)
		AutomatorApi:inputText("Hi~ 您好~")
		AutomatorApi:clickByTextEqual("发送", 0)
		
		waitNewWindow("com.tencent.mm/.ui.contact.SayHiEditUI", 10000)
	until(true)
	
	return RET_SUCCEEDED, "Add near friend succeeded."
end

--设置微信号
function WechatOps.setAlias(alias)
	local ret,msg
  local object
	
	if ALREAD_REGISTERED then 
		AutomatorApi:executeShellCommand("am start --activity-no-history com.tencent.mm/.ui.account.LoginFingerprintUI")
		mSleep(1000)
		AutomatorApi:click(623, 1220)
		
		local ret = AutomatorApi:waitNewWindowByTextContain("微信号：", 2000)
		if ret then
			local text = AutomatorApi:getTextByTextContain("微信号：", 0)
			WechatOps.strCurUsr = string.match(text, "微信号：(.*)")
			return RET_SUCCEEDED, "Set alias succeeded."
		end
	end
	
	if alias == nil then
		alias = WechatOps.strCurUsr
		local chara = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
		for var=1,4 do
			local i = math.random(1, string.len(chara))
			alias = string.sub(chara, i, i)..alias
		end
	end
	
	AutomatorApi:executeShellCommand("am start --activity-no-history com.tencent.mm/.plugin.setting.ui.setting.SettingsAliasUI")
	
	repeat
		ret = AutomatorApi:waitNewWindowByTextEqual('设置微信号', 5000)
		if ret == false then
      ret = RET_NORMAL_FAILED
			msg = "Set alias failed. Can't go to plugin.setting.ui.setting.SettingsAliasUI"
			return ret, msg
		end
		
		AutomatorApi:setTextByClass("android.widget.EditText", alias, 0)
		mSleep(800)
		AutomatorApi:clickByTextEqual("保存", 0)
    ret = AutomatorApi:waitNewWindowByTextEqual('确定', 5000)
    if ret == false then
      ret = RET_NORMAL_FAILED
			msg = "Set alias failed. Can't go to plugin.setting.ui.setting.SettingsAliasUI"
			return ret, msg
		end

    object = uiDevice:findObject(By:text('确定'))
    if object ~= nil then
      object:click()
      AutomatorApi:click(541, 773)
    else
      AutomatorApi:click(541, 773)
    end
    
		--AutomatorApi:clickByTextEqual("确定", 0)
		if AutomatorApi:waitNewWindowByTextContain("你操作频率过快", 2000) then
			ret = RET_NORMAL_FAILED
			msg = "Set alias failed. Operating frequency too fast prompt."
			return ret, msg
		end
		if AutomatorApi:waitNewWindowByTextContain("帐号已经存在", 2000) then
			return WechatOps.setAlias()
		end
		
		ret = waitNewWindow("com.tencent.mm/.plugin.setting.ui.setting.SettingsAliasUI", 25000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Set alias failed. Send time out."
			return ret, msg
		end
		WechatOps.strCurUsr = alias
	until(true)
	
	return RET_SUCCEEDED, "Set alias succeeded."
end

--设置性别
function WechatOps.setSex(sex)
	local ret,msg
	
	AutomatorApi:executeShellCommand("am start --activity-no-history com.tencent.mm/.plugin.setting.ui.setting.SettingsPersonalInfoUI")
	repeat
		ret = AutomatorApi:waitNewWindowByTextEqual('个人信息', 5000)
		ret = ret and AutomatorApi:waitNewWindowByTextEqual('性别', 2000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Set sex failed. Can't go to plugin.setting.ui.setting.SettingsPersonalInfoUI"
			return ret, msg
		end
		
		AutomatorApi:clickByTextEqual("性别", 0)
		mSleep(500)
		if sex == 'm' then
			AutomatorApi:clickByTextEqual("男", 0)
		else
			AutomatorApi:clickByTextEqual("女", 0)
		end
	until(true)
	
	return RET_SUCCEEDED, "Set sex succeeded."
end

--设置昵称
function WechatOps.setNickname(nickname)
	local ret,msg
	
	AutomatorApi:executeShellCommand("am start --activity-no-history com.tencent.mm/.plugin.setting.ui.setting.SettingsModifyNameUI")
	repeat
		ret = AutomatorApi:waitNewWindowByTextEqual('更改名字', 2000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Set nickname failed. Can't go to plugin.setting.ui.setting.SettingsModifyNameUI"
			return ret, msg
		end
		
		AutomatorApi:setTextByClass("android.widget.EditText", "", 0)
		AutomatorApi:inputText(nickname)
		mSleep(500)
		AutomatorApi:clickByTextEqual("保存", 0)
	until(true)
	
	return RET_SUCCEEDED, "Set nickname succeeded."
end

--通过微信自带浏览器打开URL
function WechatOps.openUrlByWx(url)
	AutomatorApi:executeShellCommand("am start -n com.tencent.mm/.plugin.webview.ui.tools.WebViewUI -d '"..url.."'")
	return RET_SUCCEEDED, "Open url succeeded."
end

--设置地区
function WechatOps.setArea()
	AutomatorApi:executeShellCommand("am start --activity-no-history com.tencent.mm/.plugin.setting.ui.setting.SettingsPersonalInfoUI")

	repeat
		ret = AutomatorApi:waitNewWindowByTextEqual('个人信息', 2000)
		ret = ret and AutomatorApi:waitNewWindowByTextEqual('地区', 2000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Set sex failed. Can't go to plugin.setting.ui.setting.SettingsPersonalInfoUI"
			return ret, msg
		end
		
		AutomatorApi:clickByTextEqual("地区", 0)
		mSleep(3000)
		AutomatorApi:swipe(719,1096,719,150,math.random(5,100))
		mSleep(2000)
		AutomatorApi:click(281,562)
	until(true)
	return RET_SUCCEEDED, "Set area succeeded"
end

--设置头像
function WechatOps.setHead(head_png)
	local ret,msg
	
	local file_exists = AutomatorApi:fileExists(head_png)
	if file_exists == false then
		return RET_NORMAL_FAILED, "Set head failed. File not found."
	end
	
	AutomatorApi:executeShellCommand("am start --activity-no-history com.tencent.mm/.ui.account.LoginFingerprintUI")
	mSleep(1500)
	AutomatorApi:click(625, 1223)
	mSleep(1500)
	AutomatorApi:click(220, 263)
	repeat
		ret = AutomatorApi:waitNewWindowByTextEqual('个人信息', 5000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Set head failed. Can't go to plugin.setting.ui.setting.SettingsPersonalInfoUI"
			return ret, msg
		end
		
		AutomatorApi:clickByTextEqual("头像", 0)
		ret = AutomatorApi:waitNewWindowByTextEqual('拍摄照片', 5000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Set head failed. Next UI not found."
			return ret, msg
		end
		
		mSleep(1000)
		local iRet, sRet
		AutomatorApi:clickByClass("android.widget.ImageView", 2)

		if iRet == false then
			ret = RET_NORMAL_FAILED
			msg = "Set head failed. Can't find any image."
			return ret, msg
		end
		
		ret = AutomatorApi:waitNewWindowByTextEqual('使用', 5000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Set head failed. Next UI not found."
			return ret, msg
		end
		
		AutomatorApi:executeShellCommand('am start -n com.tencent.mm/.ui.tools.CropImageNewUI  --ei CropImageMode 1 --es CropImage_ImgPath "'..head_png..'"')
		mSleep(1000)
		AutomatorApi:clickByTextEqual("使用", 0)
		
		ret = AutomatorApi:waitNewWindowByTextEqual('个人信息', 15000)
--		if ret == false then
--			ret = RET_NORMAL_FAILED
--			msg = "Set head failed. Upload failed."
--			return ret, msg
--		end
	until(true)
	
	return RET_SUCCEEDED, "Set head succeeded."

end

--设置朋友圈背景图片
function WechatOps.setSnsBackground(sns_png)
	local ret,msg
	
	local file_exists = AutomatorApi:fileExists(sns_png)
	if file_exists == false then
		ret = RET_NORMAL_FAILED
		return false, "Set sns background failed. File not found."
	end
	
	AutomatorApi:executeShellCommand('am start -n com.tencent.mm/.plugin.sns.ui.SettingSnsBackgroundUI')
	
	repeat
		ret = AutomatorApi:waitNewWindowByTextEqual('更换相册封面', 10000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Set sns background failed. Can't go to .plugin.sns.ui.SettingSnsBackgroundUI."
			return ret, msg
		end
		
		--从手机相册选择
		AutomatorApi:click(333, 240)
		
		ret = AutomatorApi:waitNewWindowByTextEqual('拍摄照片', 10000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Set sns background failed. Next UI not found."
			return ret, msg
		end
		
		mSleep(1000)
		local iRet, sRet
		AutomatorApi:clickByClass("android.widget.ImageView", 2)

		if iRet == false then
			ret = RET_NORMAL_FAILED
			msg = "Set sns background failed. Can't find any image."
			return ret, msg
		end
		
		ret = AutomatorApi:waitNewWindowByTextEqual('使用', 5000)
		if ret == false then
			ret = RET_NORMAL_FAILED
			msg = "Set sns background failed. Next UI not found."
			return ret, msg
		end
		
		AutomatorApi:executeShellCommand('am start -n com.tencent.mm/.ui.tools.CropImageNewUI  --ei CropImageMode 1 --es CropImage_ImgPath "'..sns_png..'"')
		mSleep(1000)
		AutomatorApi:clickByTextEqual("使用", 0)
		
		local time_out = 15000
		local top_activity = ""
		while time_out > 0 do
			top_activity = getTopActivity()
			if top_activity ~= "com.tencent.mm/.ui.tools.CropImageNewUI" then
				break
			end
			time_out = time_out - 500
		end
	until(true)

	return RET_SUCCEEDED, "Set sns background succeeded."
end

--添加通讯录好友
function WechatOps.addContactFriend(contacts)
	if #contacts == 0 then
		return RET_NORMAL_FAILED, "Add contact friend failed. Can't find any contacts."
	end
	
	local var
	local contacts_content = ""
	for var=1,#contacts do
		local name = contacts[var][1]
		local phone = contacts[var][2]
		contacts_content = contacts_content..name..","..phone.."\n"
	end
	AutomatorApi:writeFile(getPath().."/tmp/contacts.txt", contacts_content)
	
	clearContacts()
	mSleep(5000)
	addContacts(getPath().."/tmp/contacts.txt")
	mSleep(10000)

	local object
	local ret
	
	AutomatorApi:executeShellCommand("am start -n com.tencent.mm/.ui.bindmobile.MobileFriendUI")
	
	ret = AutomatorApi:waitNewWindowByTextEqual('提示', 3000)
	if ret then
		AutomatorApi:clickByTextEqual('确定', 0)
	end
	
	ret = AutomatorApi:waitNewWindowByTextEqual('查看手机通讯录', 10000)
	if ret == false then
		return RET_NORMAL_FAILED, "Add contact friend failed. Can't open .ui.bindmobile.MobileFriendUI."
	end
	
	local timeout = 30000
	while timeout > 0 do
		-- local count = 0
		local updated = false
		for var=1,#contacts do
			local name = contacts[var][1]
			object = uiDevice:findObject(By:text(name))
			if object ~= nil then 
				-- count = count + 1
				updated = true
				break
			end
		end
		if updated then break end
		
		AutomatorApi:executeShellCommand("am start --activity-no-history com.tencent.mm/.ui.bindmobile.MobileFriendUI")
		mSleep(1000)	
		AutomatorApi:waitNewWindowByTextEqual('查看手机通讯录', 10000)
		timeout = timeout - 1000
	end
	
	if timeout <= 0 then
		return RET_NORMAL_FAILED, "Add contact friend failed.  Can't find any contacts."
	end
	
	local addCount = 5
	local swipeCount = 10
	while addCount > 0 and swipeCount >= 0 do
		object = uiDevice:findObject(By:text('添加'))
		if object ~= nil then
			object:click()
			mSleep(1000)
			addCount = addCount - 1
		else
			AutomatorApi:swipe(719,1279,719,350, 50)
			swipeCount = swipeCount -1
			mSleep(1000)
		end
	end
	
	return RET_SUCCEEDED, 'Add contact friend succeeded.'
end

--分享链接
function WechatOps.shareUrlLink(url)
	local ret, object
	
	WechatOps.openUrlByWx('http://w.t.qq.com/wuxian/home/guest?id=pinganbeijing')
	mSleep(10000)
	AutomatorApi:click(674, 94)
	ret = AutomatorApi:waitNewWindowByTextEqual('分享到朋友圈', 1500)
	if ret == false then
		return RET_NORMAL_FAILED, "Share url failed. Can't find share button."
	end
	AutomatorApi:clickByTextEqual('分享到朋友圈', 0)
	
	ret = AutomatorApi:waitNewWindowByTextEqual('发送', 10000)
	if ret == false then
		return RET_NORMAL_FAILED, "Share url failed. Can't go to send ui."
	end
	
	object = uiDevice:findObject(By:clazz('android.widget.TextView'):clickable(false))
	if object == nil then
		return RET_NORMAL_FAILED, "Share url failed. Can't get the url text."
	end
	local text = object:getText()
	
	AutomatorApi:clickByTextEqual('发送', 0)
	mSleep(5000)
	
	AutomatorApi:executeShellCommand("am start -n com.tencent.mm/.plugin.sns.ui.SnsTimeLineUI")
	
	ret = AutomatorApi:waitNewWindowByTextEqual('朋友圈', 5000)
	if ret == false then
		return RET_NORMAL_FAILED, "Share url failed. Can't go to time line ui."
	end
	AutomatorApi:click(620, 653)
	
	ret = AutomatorApi:waitNewWindowByTextEqual('我的相册', 5000)
	if ret == false then
		return RET_NORMAL_FAILED, "Share url failed. Can't go to my camera ui."
	end
	
	mSleep(5000)
	ret = AutomatorApi:waitNewWindowByTextEqual(text, 5000)
	if ret == false then
		return RET_NORMAL_FAILED, "Share url failed. Send failed."
	end
	AutomatorApi:clickByTextEqual(text, 0)
	
	ret = AutomatorApi:waitNewWindowByTextEqual('详情', 5000)
	if ret == false then
		return RET_NORMAL_FAILED, "Share url failed. Open Detail ui failed."
	end
	
	return RET_SUCCEEDED, "Share url succeeded."
end

--投票
function WechatOps.vote()
	local ret, msg
	func = dofile(getPath().."/lua/wx_vote.lua")
	ret,msg = func()
	return ret, msg
end

--所有操作结束后执行保存缓存的操作
function WechatOps.doEnd()
  if SharedDic.get('test_wx_switch') == 'true' then
    local original_cache_path = '/data/data/com.tencent.mm'
    local cache_file = getPath().."/res/cache/com.tencent.mm/"..WechatOps.strCurUsr.."_"
    local usr_new_cache_path = original_cache_path..'_'..WechatOps.strCurUsr
    
    --将绑定的设备信息文件和密码保存
    AutomatorApi:executeShellCommand("mkdir -p "..cache_file)
    AutomatorApi:executeShellCommand("cp -a "..usr_new_cache_path.."/param.properties "..cache_file)
    AutomatorApi:executeShellCommand("cp -a "..usr_new_cache_path.."/password "..cache_file)
    return RET_SUCCEEDED, "End succeeded."
  end
  
	if WechatOps.strCurUsr ~= "" then  
		AutomatorApi:executeShellCommand("am force-stop com.tencent.mm")
		mSleep(3000)
    
    local cache_file = getPath().."/res/cache/com.tencent.mm/"..WechatOps.strCurUsr
    local task_type = SharedDic.get("task_type")
    if task_type == 'wx_register' then
      local original_cache_path = '/data/data/com.tencent.mm'
      local usr_cache_path = '/data/data/register_com.tencent.mm'
      local usr_new_cache_path = original_cache_path..'_'..WechatOps.strCurUsr 
      AutomatorApi:executeShellCommand('mv '..usr_cache_path..' '..usr_new_cache_path)
      --重新建立软连接到指定登录账号
      AutomatorApi:executeShellCommand('busybox ln -sf '..usr_new_cache_path..' '..original_cache_path)
      
      --将绑定的设备信息文件和密码保存
      AutomatorApi:executeShellCommand("mkdir -p "..cache_file)
      AutomatorApi:executeShellCommand("cp -a "..usr_new_cache_path.."_/param.properties "..cache_file)
      AutomatorApi:executeShellCommand("cp -a "..usr_new_cache_path.."_/password "..cache_file)
    else
      AutomatorApi:executeShellCommand("rm -fr "..cache_file.."*")
      AutomatorApi:executeShellCommand("mkdir -p "..cache_file.."/com.tencent.mm/")
      AutomatorApi:executeShellCommand("mkdir -p "..cache_file.."/tencent/MicroMsg/")
      AutomatorApi:executeShellCommand('cp -a '..getPath()..'/res/param.properties '..cache_file..'/')
      AutomatorApi:executeShellCommand("cp -a /data/data/com.tencent.mm/* "..cache_file.."/com.tencent.mm/")
      AutomatorApi:executeShellCommand("cp -a /sdcard/tencent/MicroMsg/* "..cache_file.."/tencent/MicroMsg/")
      AutomatorApi:executeShellCommand("rm -fr "..cache_file.."/com.tencent.mm/app_lib")
      AutomatorApi:executeShellCommand("rm -fr "..cache_file.."/com.tencent.mm/app_cache")
      AutomatorApi:executeShellCommand("rm -fr "..cache_file.."/com.tencent.mm/app_dex")
      AutomatorApi:executeShellCommand("rm -fr "..cache_file.."/tencent/MicroMsg/*.apk")
      AutomatorApi:executeShellCommand("busybox tar -zcf "..cache_file..".tar.gz "..WechatOps.strCurUsr.." -C "..getPath().."/res/cache/com.tencent.mm/")
    end
	
		if SharedDic.get('test_wx_switch') == 'false' and task_type ~= 'wx_register' then
			TaskInterface.uploadCacheFile(cache_file..".tar.gz")
		end
	end
	return RET_SUCCEEDED, "End succeeded."
end

--根据指令执行任务
function WechatOps.doTask(data)
	local cmd = data[1]
	local ret, msg
	
	if cmd == CMD_WX_LOGIN then
		ret, msg = WechatOps.login(data[2], data[3])
	elseif cmd == CMD_WX_REGISTER then
		ret, msg = WechatOps.register(data[2], data[3], data[4], data[5])
	else
		repeat
			if WechatOps.strCurUsr == "" then
				ret, msg = RET_NORMAL_FAILED, "not login!"
				break
			end

			if cmd == CMD_WX_ADD_FRIEND then
				local ret_tmp, msg_tmp
				local tbl_usrs = string.split(data[2], ',')
				
				if tbl_usrs == nil or #tbl_usrs == 0 then
					msg = "User empty."
				else
					msg = ''
					for k,v in pairs(tbl_usrs) do
						ret_tmp, msg_tmp = WechatOps.addFriend(v, data[3])
						msg = msg..'>'..msg_tmp
					end
				end
				ret = RET_SUCCEEDED
			elseif cmd == CMD_WX_SEND_MSG then
				ret, msg = WechatOps.sendMsg(data[2], data[3])
			elseif cmd == CMD_WX_ADD_MP then
				ret, msg = WechatOps.addMp(data[2])
			elseif cmd == CMD_WX_VOTE then
				ret, msg = WechatOps.vote()
			elseif cmd == CMD_WX_SEND_SNS then
				ret, msg = WechatOps.sendSnsJustText(data[2])
			elseif cmd == CMD_WX_UPDATE_NICKNAME then
				ret, msg = WechatOps.setNickname(data[2])
			elseif cmd == CMD_WX_ADD_NEAR_FRIEND then
				ret, msg = WechatOps.addNearFriend()
			elseif cmd == CMD_WX_UPDATE_SEX then
				ret, msg = WechatOps.setSex(data[2])
			elseif cmd == CMD_WX_RANDOM_SEND_SNS then
				--ret, msg = randomSendSNS()
			elseif cmd == CMD_WX_CLEAR_MSG_RECORD then
				ret, msg = WechatOps.clearRecorder(data[2])
			elseif cmd == CMD_WX_SET_ALIAS then
				ret, msg = WechatOps.setAlias(data[2])
			elseif cmd == CMD_WX_END then
				ret, msg = WechatOps.doEnd()
			elseif cmd == CMD_WX_OPEN_URL_BY_WX then
				ret, msg = WechatOps.openUrlByWx(data[2])
			elseif cmd == CMD_WX_SET_AREA then
				ret, msg = WechatOps.setArea()
			elseif cmd == CMD_WX_SEND_SNS_WITH_PICTURE then
				if data[3] == "" then
					ret, msg = WechatOps.sendSnsJustText(data[2])
				else
					ret, msg = WechatOps.sendSnsWithPicture(data[2], getPath().."/tmp/gh_task/"..SharedDic.get("task_id").."/"..data[3])
				end
			elseif cmd == CMD_WX_SET_HEAD then
				if data[2] ~= "" then
					local head_png = getPath().."/tmp/gh_task/"..SharedDic.get("task_id").."/"..data[2]
					if SharedDic.get('test_wx_switch') == 'true' then head_png = data[2] end
					ret, msg = WechatOps.setHead(head_png)
				end
			elseif cmd == CMD_WX_SET_SNS_BACKGROUND then
				if data[2] ~= "" then
					local sns_png = getPath().."/tmp/gh_task/"..SharedDic.get("task_id").."/"..data[2]
					if SharedDic.get('test_wx_switch') == 'true' then sns_png = data[2] end
					ret, msg = WechatOps.setSnsBackground(sns_png)
				end
			elseif cmd == CMD_WX_ADD_CONTACT_FRIEND then
				ret, msg = WechatOps.addContactFriend(data[2])
			elseif cmd == CMD_WX_SHARE_URL then
				ret, msg = WechatOps.shareUrlLink(data[2])
			end
		until (true)
	end
	
	AutomatorApi:log("wechat", msg)

	return ret, msg
end