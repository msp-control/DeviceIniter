require 'FacebookOps'

function testFbSetProfile()
  local ret, msg
  local cur_fb_email
  local iRet,sRet = pcall(function () 
    cur_fb_email = AutomatorApi:readFile(getPath().."/tmp/cur_fb_email.txt")
  end)

  if iRet == false then
    AutomatorApi:toast("当前没有产生成功注册账号")
    AutomatorApi:logAppend("FbSetProfile", "当前没有产生成功注册账号")
    mSleep(2000)
  else
    cur_fb_email = string.gsub(cur_fb_email, "\n", "")
    local profile_file = getPath().."/tmp/fb_info/"..cur_fb_email.."/profile.txt"
    
    local file_exists = AutomatorApi:fileExists(profile_file)
    if file_exists == false then
      AutomatorApi:toast(profile_file.." 文件不存在")
      AutomatorApi:logAppend("FbSetProfile", profile_file.." 文件不存在")
      mSleep(2000)
    end
    
    local work = AutomatorApi:getProperty(profile_file, "work", "")
    local college = AutomatorApi:getProperty(profile_file, "college", "")
    local high_school = AutomatorApi:getProperty(profile_file, "high_school", "")
    local city = AutomatorApi:getProperty(profile_file, "city", "")
    local town = AutomatorApi:getProperty(profile_file, "town", "")
    
    if city == "" then
      AutomatorApi:logAppend("FbSetProfile", "city null")
      return
    end
    
    local msg_tmp
    msg = ""
    if work ~= "" then
      msg_tmp = ""
      local iRet, sRet = pcall(function()
        ret, msg_tmp = FacebookOps.setWork(work, city)
      end)
      if iRet == false then
        msg_tmp = sRet
      end
      msg = msg.."|"..msg_tmp
      AutomatorApi:toast(msg_tmp)
    end
    
    if college ~= "" then
      msg_tmp = ""
      local iRet, sRet = pcall(function()
        ret, msg_tmp = FacebookOps.setCollege(college, city)
      end)
      if iRet == false then
        msg_tmp = sRet
      end
      msg = msg.."|"..msg_tmp
      AutomatorApi:toast(msg_tmp)
    end
    
    if high_school ~= "" then
      msg_tmp = ""
      local iRet, sRet = pcall(function()
        ret, msg_tmp = FacebookOps.setHighSchool(high_school, city)
      end)
      if iRet == false then
        msg_tmp = sRet
      end
      msg = msg.."|"..msg_tmp
      AutomatorApi:toast(msg_tmp)
    end

    if city ~= "" then
      msg_tmp = ""
      local iRet, sRet = pcall(function()
        ret, msg_tmp = FacebookOps.setPlace(city, town)
      end)
      if iRet == false then
        msg_tmp = sRet
      end
      msg = msg.."|"..msg_tmp
      AutomatorApi:toast(msg_tmp)
    end
    
    AutomatorApi:logAppend("FbSetProfile", msg)
    mSleep(200)
  end
end

testFbSetProfile()
AutomatorApi:executeShellCommand("busybox ps -ef|busybox grep app_process |busybox grep -v 'grep'|busybox grep instrument|busybox awk '{print $1}'|busybox xargs kill -9")
