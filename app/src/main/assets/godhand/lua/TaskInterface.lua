TaskInterface = {}


--服务器配置信息
local g_ssl_flag = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'ssl_switch', 'false')
local g_url_pre = g_ssl_flag == "true" and "https://" or "http://"
local g_conf_remote_ip = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'server_ip', '192.168.70.97')
local g_conf_remote_port = AutomatorApi:getProperty(getPath()..'/res/conf.pro', 'server_port', '3002')
local g_conf_host = g_url_pre..g_conf_remote_ip..":"..g_conf_remote_port
local g_conf_task_result_url = g_conf_host.."/task_status"
local g_conf_task_file_url = g_conf_host.."/task_file"
local g_conf_download_complete_url = g_conf_host.."/download_complete"
local g_conf_check_update_url = g_conf_host.."/check_upgrade"
local g_conf_update_url = g_conf_host.."/upgrade"
local g_conf_get_vcode_url = g_conf_host.."/get_vcode"
local g_conf_put_vcode_url = g_conf_host.."/put_vcode"
local g_conf_send_phone_url = g_conf_host.."/send_phone"
local g_conf_get_sms_url = g_conf_host.."/get_sms"
local g_conf_upload_file_url = g_conf_host.."/upload"

--检查更新
function TaskInterface.check_update(version)
  local res = AutomatorApi:executeShellCommand("curl -k -s \""..g_conf_check_update_url.."?version="..version.."\"")
--	local res = AutomatorApi:httpGet(g_conf_check_update_url, "version="..version)
  return res
end

--下载更新包
function TaskInterface.downloadUpgradeFile(update_file_recv_path)
  AutomatorApi:executeShellCommand("curl -k -s -o "..update_file_recv_path.." \""..g_conf_update_url.."\"")
end

--下载任务包
function TaskInterface.downloadTaskFile(task_file_recv_path, guid)
  AutomatorApi:executeShellCommand("curl -k -s -o '"..task_file_recv_path.."' \""..g_conf_task_file_url.."?guid="..guid.."\"")
end

--通知下载任务完成
function TaskInterface.downloadComplete(guid, task_id)
  local taskid = task_id or ''
--	AutomatorApi:httpGet(g_conf_download_complete_url, "guid="..guid..'&task_id='..taskid)
  AutomatorApi:executeShellCommand("curl -k -s \""..g_conf_download_complete_url.."?guid="..guid..'&task_id='..task_id.."\"")
end


--任务结束结果返回
function TaskInterface.postTaskResult(result)
  local str_post = json.encode(result)
  AutomatorApi:writeFile(getPath().."/tmp/postResult.txt", str_post)
  
  local repeat_times = 0
  local sRet = ""
  while true do
    sRet = AutomatorApi:executeShellCommand("curl -H 'Content-Type: application/json' -w%{http_code} -o "..getPath().."/tmp/postResultRes.txt"
      .." -k -s -d @"..getPath().."/tmp/postResult.txt "..g_conf_task_result_url.."| busybox tail -1")
    repeat_times = repeat_times + 1
    if string.find(sRet, '200') ~= nil then
      local resp = AutomatorApi:readFile(getPath().."/tmp/postResultRes.txt")
      if resp ~= nil and string.find(resp, '{"success":true}') then
        break
      end
    else
      AutomatorApi:log("http_err", "postTaskResult: "..tostring(repeat_times)..str_post)
    end
    mSleep(3000)
  end
--  local headers = 'Content-Type:application/json;Content-Length:'..#str_post  
--  local repeat_times = 0
--  local sRet = ""
--  while true do
--    sRet = AutomatorApi:httpPostWithHeader(g_conf_task_result_url, str_post, headers)
--    repeat_times = repeat_times + 1
--    if sRet ~= "" then
--      break
--    else
--      AutomatorApi:log("http_err", "postTaskResult: "..tostring(repeat_times)..str_post)
--    end
--    mSleep(3000)
--  end
  AutomatorApi:log("http_suc_full_test", "postTaskResult: "..str_post)
  AutomatorApi:logAppend("http_suc", "postTaskResult: "..result["message"])
  AutomatorApi:executeShellCommand("rm -f "..getPath().."/tmp/task_file.tar")
end

--上传缓存文件
function TaskInterface.uploadCacheFile(cache_file)
  local repeat_times = 5
  local curl_ret = ""
  local curl_tmp_file = getPath().."/tmp/curl_tmp.txt"
  local cmd = "curl -k --connect-timeout 5 -o /dev/null -w %{http_code} -F 'filename=@"..cache_file.."' "
      ..g_conf_upload_file_url.."?task_id="..SharedDic.get("task_id")
  repeat	
    curl_ret = AutomatorApi:executeShellCommand(cmd)
    repeat_times = repeat_times - 1
    AutomatorApi:toast(curl_ret..repeat_times)
  until (curl_ret=='200' and repeat_times >= 0)
  
  if string.find(curl_ret, '{"success":true}') ~= nil then
    AutomatorApi:logAppend("http_err", "uploadCacheFile: "..cmd)
  else
    AutomatorApi:logAppend("http_suc", "uploadCacheFile: "..curl_ret)
  end
end

--获取图片验证码
function TaskInterface.getVcode(guid, task_id, file, timeout)
	local str_post = {
			["guid"] = guid,
			["task_id"] = task_id,
			["snapshot_path"] = AutomatorApi:base64EncodeFile(file)
		}
		
	str_post = json.encode(str_post)
	local sRet = AutomatorApi:httpPost(g_conf_put_vcode_url, str_post)
	if sRet == '{"success":true}' then
		str_post = {
			["guid"] = guid,
			["task_id"] = task_id
		}
		
		str_post = json.encode(str_post)
		while timeout > 0 do
			sRet = AutomatorApi:httpPost(g_conf_get_vcode_url, str_post)
			if sRet ~= "" then
				local iRet, sRet = pcall(function()
						return json.decode(sRet)
				end)
				
				if iRet == true then
					if sRet["success"] == true then
						local code = sRet["vcode"]
						return true, code
					elseif sRet["success"] == false then
						AutomatorApi:mSleep(2000)
						timeout = timeout - 2000
						if timeout <= 0 then
							return false, "getVcode time out."..sRet["message"]
						end
					end
				else
					return false, "Get vcode failed. Json decode error."
				end
			else
				AutomatorApi:log("main", "Get vcode failed. Post failed.")
				return false, "Get vcode failed. Post failed."
			end
		end
	else
		return false, "Put vcode failed."
	end
end

--获取短信验证码
function TaskInterface.getSms(guid, task_id, phone_num, timeout)
	local http_result_file = getPath().."/tmp/http_result.txt"
	local str_post = {
			["guid"] = guid,
			["task_id"] = task_id,
			["phone"] = phone_num
		}
		
	str_post = json.encode(str_post)
	AutomatorApi:executeShellCommand("curl -d '"..str_post
		.."' -o "..http_result_file
		.." -H 'Content-Type: application/json'"
		.." -H 'Content-Length: "..#str_post.."' "
		..g_conf_send_phone_url)
	local sRet = AutomatorApi:executeShellCommand("cat "..http_result_file)
	if sRet == '{"success":true}' then
		str_post = {
			["guid"] = guid,
			["task_id"] = task_id
		}
		
		str_post = json.encode(str_post)
		while timeout > 0 do
			sRet = AutomatorApi:executeShellCommand("curl -d '"..str_post
				.."' -o "..http_result_file
				.." -H 'Content-Type: application/json'"
				.." -H 'Content-Length: "..#str_post.."' "
				..g_conf_get_sms_url)
			sRet = AutomatorApi:readFile(http_result_file)
			if sRet ~= "" then
				local iRet, sRet = pcall(function()
						return json.decode(sRet)
				end)
				
				if iRet == true then
					if sRet["success"] == true then
						local sms = sRet["sms"]
						return true, sms
					elseif sRet["success"] == false then
						AutomatorApi:mSleep(2000)
						timeout = timeout - 2000
						if timeout <= 0 then
							return false, "getSms time out."..sRet["message"]
						end
					end
				else
					AutomatorApi:log("main", "getSms().get_sms failed. Json decode error.")
					return false, "Get vcode failed. Json decode error."
				end
			else
				AutomatorApi:log("main", "getSms().get_sms failed. Post failed.")
				return false, "Get sms failed. Post failed."
			end
		end
	else
		AutomatorApi:log("main", "getSms().send_phone failed."..sRet)
		return false, "Send phone failed."
	end
end