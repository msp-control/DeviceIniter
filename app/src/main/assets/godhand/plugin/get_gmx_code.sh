#!/system/bin/sh

if [ $# -ne 3 -a $# -ne 4 ]
then
	echo "Usage: sh $0 name password timeout [mark]" >&2
	exit
fi

#登录
curl -k -s -D/sdcard/GodHand/log/head.txt -o/sdcard/GodHand/log/gmx_login.log \
	 -H 'Connection: keep-alive' \
	 -H 'Cache-Control: max-age=0' \
	 -H 'Upgrade-Insecure-Requests: 1' \
	 -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36' \
	 -H 'Content-Type: application/x-www-form-urlencoded' \
	 -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' \
	 -H 'Referer: https://www.gmx.com/' \
	 -H 'Accept-Language: zh-CN,zh;q=0.8' \
	 -H 'Accept-Encoding: gzip, deflate, br' \
	 -H 'Cookie: __gads=ID=bffdc2936ca863c3:T=1474945100:S=ALNI_MaBBDzpwyVQO7pBjU55FLviC7VFaA; ns_sample=43; cookieKID=kid%40autoref%40gmx.com; cookiePartner=kid%40autoref%40gmx.com' \
	 -d "service=mailint&uasServiceID=mc_starter_gmxcom&successURL=https%3A%2F%2F%24%28clientName%29-%24%28dataCenter%29.gmx.com%2Flogin&loginFailedURL=https%3A%2F%2Fwww.gmx.com%2Flogout%2F%3Fls%3Dwd&loginErrorURL=https%3A%2F%2Fwww.gmx.com%2Flogout%2F%3Fls%3Dte&edition=us&lang=en&usertype=standard&username=$1&password=$2" \
	 "https://login.gmx.com/login"

busybox dos2unix /sdcard/GodHand/log/head.txt > /dev/null
navigatorUrl=`cat /sdcard/GodHand/log/head.txt|grep 'ocation: '|busybox awk -F'ocation: ' '{print $2}'`	 
ott=`cat /sdcard/GodHand/log/head.txt | grep 'ott='|busybox awk -F'ott=' '{print $2}'`
if [ "${ott}" = "" ]
then
	echo "failed. ott null."
	exit
fi

rm -f /sdcard/GodHand/log/cookie.txt
curl -k -s -L -c/sdcard/GodHand/log/cookie.txt -b/sdcard/GodHand/log/cookie.txt -D/sdcard/GodHand/log/head.txt -o/sdcard/GodHand/log/body.gz \
	 -H 'Connection: keep-alive' \
	 -H 'Upgrade-Insecure-Requests: 1' \
	 -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36' \
	 -H 'Content-Type: application/x-www-form-urlencoded' \
	 -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' \
	 -H "Referer: $navigatorUrl" \
	 -H 'Accept-Language: zh-CN,zh;q=0.8' \
	 -H 'Accept-Encoding: gzip, deflate, br' \
	 "https://navigator-bs.gmx.com/navigator/fd/evaluate?edition=us&lang=en&uasServiceID=mc_starter_gmxcom&usertype=standard&ott=${ott}&tz=8&fd=%7B%22cookies%22%3Atrue%2C%22postmessage%22%3Atrue%2C%22flexbox%22%3Atrue%2C%22objectfreeze%22%3Atrue%2C%22blacklist%22%3Afalse%2C%22browserversion%22%3Atrue%2C%22compatibility%22%3Afalse%2C%22mobile%22%3Afalse%7D"

busybox gunzip -f /sdcard/GodHand/log/body.gz
jsessionid=`cat /sdcard/GodHand/log/body | grep 'jsessionid=' | busybox awk -F'jsessionid=' '{print $2}' | busybox awk -F'?' '{print $1}'`
if [ "$jsessionid" = "" ]
then
	echo "failed. jsessionid null."
	exit
fi

#循环获取验证码
timeout=$3
while(($timeout>0))
do
curl -k -s -c/sdcard/GodHand/log/cookie.txt -b/sdcard/GodHand/log/cookie.txt -D/sdcard/GodHand/log/head.txt -o/sdcard/GodHand/log/body.gz \
	 -H 'Connection: keep-alive' \
	 -H 'Upgrade-Insecure-Requests: 1' \
	 -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36' \
	 -H 'Host: 3c-bs.gmx.com' \
	 -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' \
	 -H "Referer: https://3c-bs.gmx.com/mail/client/mail/debusybox tail;jsessionid=$jsessionid?2&mailId=tmai147821ce559e856f&page=0&sort=sort-DATE-desc&folderId=tfol" \
	 -H 'Accept-Language: zh-CN,zh;q=0.8' \
	 -H 'Accept-Encoding: gzip, deflate, br' \
	 "https://3c-bs.gmx.com/mail/client/folder/unread;jsessionid=$jsessionid?folderId=tfol&uc=UNREAD_MAILS"

busybox gunzip -f /sdcard/GodHand/log/body.gz
fbmailid=`cat /sdcard/GodHand/log/body | grep -E -B 1000 '完成注册，玩转 Facebook！|registration@facebookmail.com'|grep 'data-oao-mailid=' | busybox tail -1 | busybox awk -F'data-oao-mailid="' '{print $2}' | busybox awk -F'"' '{print $1}'`
if [ "$fbmailid" != "" ]
then
	break
fi
echo "Email empty. Try again." >&2
timeout=$(($timeout-1))
sleep 1
done

if [ $timeout -eq 0 ]
then
	echo "tiemout"
	exit
fi

#读取验证码
curl -k -s -c/sdcard/GodHand/log/cookie.txt -b/sdcard/GodHand/log/cookie.txt -D/sdcard/GodHand/log/head.txt -o/sdcard/GodHand/log/body.gz \
	 -H 'Connection: keep-alive' \
	 -H 'Upgrade-Insecure-Requests: 1' \
	 -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36' \
	 -H 'Host: 3c-bs.gmx.com' \
	 -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' \
	 -H "Referer: https://3c-bs.gmx.com/mail/client/mail/debusybox tail;jsessionid=$jsessionid?2&mailId=$fbmailid&page=0&sort=sort-DATE-desc&folderId=tfol" \
	 -H 'Accept-Language: zh-CN,zh;q=0.8' \
	 -H 'Accept-Encoding: gzip, deflate, br' \
	 "https://3c-bs.gmx.com/mail/client/mailbody/$fbmailid/true"

busybox gunzip -f /sdcard/GodHand/log/body.gz
vcode=`cat /sdcard/GodHand/log/body| grep 'gmx.com%26c%3D'| busybox tail -1|busybox awk -F'gmx.com%26c%3D' '{print $2}'|busybox awk -F '%26' '{print $1}'`
echo $vcode

if [ "$4" = "mark" ]
then
#标记为已读
curl -k -s -c/sdcard/GodHand/log/cookie.txt -b/sdcard/GodHand/log/cookie.txt -D/sdcard/GodHand/log/head.txt -o/sdcard/GodHand/log/body.gz \
	 -H 'Connection: keep-alive' \
	 -H 'Upgrade-Insecure-Requests: 1' \
	 -H 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36' \
	 -H 'Host: 3c-bs.gmx.com' \
	 -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' \
	 -H "Referer: https://3c-bs.gmx.com/mail/client/mail/debusybox tail;jsessionid=$jsessionid?2&mailId=$fbmailid&page=0&sort=sort-DATE-desc&folderId=tfol" \
	 -H 'Accept-Language: zh-CN,zh;q=0.8' \
	 -H 'Accept-Encoding: gzip, deflate, br' \
	 "https://3c-bs.gmx.com/mail/client/mail/detail;jsessionid=$jsessionid?20&mailId=$fbmailid&page=0&sort=sort-DATE-desc&folderId=tfol146d1a82f3a85d8e"
fi
 
exit
